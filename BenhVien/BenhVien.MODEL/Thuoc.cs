//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BenhVien.MODEL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Thuoc
    {
        public int Id { get; set; }
        public string Ten { get; set; }
        public Nullable<int> TrangThaiID { get; set; }
        public Nullable<decimal> Gia { get; set; }
        public string DonVi { get; set; }
        public string CongTy { get; set; }
        public string MoTa { get; set; }
        public Nullable<int> GiamGiaBHYT { get; set; }
        public string AnhDaiDien { get; set; }
    }
}
