﻿using BASICAUTHORIZE.AGRICH;
using BenhVien.MODEL;
using BenhVien.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BenhVien.UI.Controllers
{
    [RouteArea("QuanTri", AreaPrefix = "admin")]
    [Route("{action}")]
    public class DanhMucDichVuController : Controller
    {
        public BenhVienEntities db = new BenhVienEntities();
        [Route("danh-muc-dich-vu/main-page")]
        public ActionResult MainPage()
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });
            return View();
        }
        [Route("danh-muc-dich-vu/list")]
        public ActionResult List(string keyword = "", int? status = EnumStatus.ACTIVE, int sotrang = 1, int tongsodong = 5)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            if (keyword != "")
                keyword = keyword.BoDauTiengViet().ToLower();

            var list = (from a in db.LoaiDichVus.ToList()
                        where (keyword == "" || (keyword != "" && (a.Ten.BoDauTiengViet().ToLower().Contains(keyword))
                        || (a.MaLoaiDichVu.BoDauTiengViet().ToLower().Contains(keyword))

                        )
                        )
                        && (status == null ? true : a.TrangThaiID == status)
                        select a).OrderBy(x => x.Id);

            int tongso = list.Count();

            sotrang = sotrang <= 0 ? 1 : sotrang;
            tongsodong = tongsodong <= 0 ? 10 : tongsodong;
            int tongsotrang = tongso % tongsodong > 0 ? tongso / tongsodong + 1 : tongso / tongsodong;
            tongsotrang = tongsotrang <= 0 ? 1 : tongsotrang;
            sotrang = sotrang > tongsotrang ? tongsotrang - 1 : sotrang - 1;
            ViewBag.sotrang = sotrang + 1;
            ViewBag.tongsotrang = tongsotrang;
            ViewBag.tongso = tongso;
            return PartialView(list == null ? list : list.Skip(sotrang * tongsodong).Take(tongsodong));
        }
        [Route("danh-muc-dich-vu/update")]
        public ActionResult Update(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var loaiDichVu = db.LoaiDichVus.FirstOrDefault(x => x.Id == id);
            return PartialView(loaiDichVu);
        }
        [Route("danh-muc-dich-vu/update")]
        [HttpPost, ValidateInput(false)]
        public ActionResult Update(LoaiDichVu loaiDichVu, HttpPostedFileBase _Logo = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });
            if (loaiDichVu.Id == 0)
            {
                loaiDichVu.MaLoaiDichVu = Functions.CreateCode();
                db.LoaiDichVus.Add(loaiDichVu);
                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // up date
                var old = db.LoaiDichVus.FirstOrDefault(x => x.Id == loaiDichVu.Id);
                if (old == null)
                    return Json(new { kq = "err", msg = "Thông tin không xác định!" }, JsonRequestBehavior.AllowGet);
                old.Ten = loaiDichVu.Ten;
                old.TrangThaiID = loaiDichVu.TrangThaiID;
                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
        }


        [Route("danh-muc-dich-vu/delete")]
        public ActionResult Delete(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var loaiDichVu = db.LoaiDichVus.FirstOrDefault(x => x.Id == id);
            if (loaiDichVu == null)
                return Json(new { kq = "err", msg = "Không xác định!" }, JsonRequestBehavior.AllowGet);

            db.LoaiDichVus.Remove(loaiDichVu);
            db.SaveChanges();
            return Json(new { kq = "ok", msg = "Thành công!" }, JsonRequestBehavior.AllowGet);
        }

        [Route("danh-muc-dich-vu/change-status")]
        public ActionResult Change_Status(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var loaiDichVu = db.LoaiDichVus.FirstOrDefault(x => x.Id == id);
            if (loaiDichVu == null)
                return Json(new { kq = "err", msg = "Không xác định!" }, JsonRequestBehavior.AllowGet);

            if (loaiDichVu.TrangThaiID == EnumStatus.ACTIVE)
                loaiDichVu.TrangThaiID = EnumStatus.INACTIVE;
            else
                loaiDichVu.TrangThaiID = EnumStatus.ACTIVE;
            db.SaveChanges();
            return Json(new { kq = "ok", data = loaiDichVu.TrangThaiID, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }
    }
}