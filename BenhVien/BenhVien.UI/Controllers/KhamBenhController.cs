﻿using BASICAUTHORIZE.AGRICH;
using BenhVien.MODEL;
using BenhVien.UI.Models;
using Microsoft.Office.Interop.Excel;
using Syncfusion.ExcelToPdfConverter;
using Syncfusion.Pdf;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BenhVien.UI.Controllers
{
    [RouteArea("QuanTri", AreaPrefix = "admin")]
    [Route("{action}")]
    public class KhamBenhController : Controller
    {
        public BenhVienEntities db = new BenhVienEntities();
        [Route("kham-benh/dang-ky")]
        public ActionResult DangKy()
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });

            string cbxBenhNhan = "<option value=\"\">Chọn bệnh nhân...</option>";
            var benhNhan = db.NguoiDungs.Where(x => x.TrangThaiID == EnumStatus.ACTIVE && x.LoaiNguoiDung == EnumUserType.BENH_NHAN).ToList();
            foreach (var item in benhNhan)
            {
                cbxBenhNhan += string.Format("<option value=\"{0}\">{1}</option>", item.Id, item.HoTen + " - " + item.SDT);
            }
            ViewBag.cbxBenhNhan = cbxBenhNhan;

            return View();
        }
        [Route("kham-benh/dang-ky-list")]
        public ActionResult DangKy_List(int? status = EnumTrangThaiKhamBenh.DANG_KY, int? id = null, string fromDate = "", string toDate = "", int sotrang = 1, int tongsodong = 5)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            DateTime? dateFrom = fromDate.ToDateTime();
            DateTime? dateTo = toDate.ToDateTime();
            var list = (from a in db.KhamBenhs.ToList()
                        join b in db.NguoiDungs on a.NguoiDungID equals b.Id into b1
                        from b in b1.DefaultIfEmpty()
                        join c in db.ChiTietKhamBenhs on a.Id equals c.KhamBenhID into c1
                        from c in c1.DefaultIfEmpty()
                        where
                        b != null &&
                        (id == null ? true : a.NguoiDungID == id)
                        && ((nd_dv.User.LoaiNguoiDung == EnumUserType.SUB_ADMIN || nd_dv.User.LoaiNguoiDung == EnumUserType.ADMIN)?true: nd_dv.User.Id == c.BacSiID)
                        &&
                        a.TrangThaiID == status
                        select new DangKyKhamBenh
                        {
                            NguoiDung = b,
                            KhamBenh = a,
                            ChiTietKhamBenh = c
                        }
                        )
                        //.Where(a => (string.IsNullOrEmpty(fromDate) ? true : a.KhamBenh.NgayKham>= dateFrom)
                        //    && (string.IsNullOrEmpty(toDate) ? true : a.KhamBenh.NgayKham.Value <= dateTo))
                        .OrderByDescending(x => x.KhamBenh.NgayKham).ToList();

            int tongso = list.Count();

            sotrang = sotrang <= 0 ? 1 : sotrang;
            tongsodong = tongsodong <= 0 ? 10 : tongsodong;
            int tongsotrang = tongso % tongsodong > 0 ? tongso / tongsodong + 1 : tongso / tongsodong;
            tongsotrang = tongsotrang <= 0 ? 1 : tongsotrang;
            sotrang = sotrang > tongsotrang ? tongsotrang - 1 : sotrang - 1;
            ViewBag.sotrang = sotrang + 1;
            ViewBag.tongsotrang = tongsotrang;
            ViewBag.tongso = tongso;
            return PartialView(list == null ? list : list.Skip(sotrang * tongsodong).Take(tongsodong));
        }

        [Route("kham-benh/benh-nhan-update")]
        public ActionResult BenhNhan_Update(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });

            var dangky = (from a in db.KhamBenhs.ToList()
                          join b in db.NguoiDungs on a.NguoiDungID equals b.Id into b1
                          from b in b1.DefaultIfEmpty()
                          where a.Id == id
                          select new DangKyKhamBenh
                          {
                              NguoiDung = b,
                              KhamBenh = a
                          }
                     )
                     .OrderByDescending(x => x.KhamBenh.NgayKham).FirstOrDefault();

            int? stt = 0;
            if (dangky == null || dangky.KhamBenh == null)
            {
                var khambenh = db.KhamBenhs.OrderByDescending(x => x.NgayKham).FirstOrDefault();
                if (khambenh == null)
                    stt = 1;
                else
                    stt = khambenh.STT + 1;
            }
            else
            {
                stt = dangky.KhamBenh.STT;
            }
            ViewBag.STT = stt;
            string cbxProvince = "<option value=\"\">Chọn tỉnh/ thành phố</option>";
            var province = db.Provinces.OrderByDescending(x => x.STT).ToList();
            foreach (var item in province)
            {
                cbxProvince += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Name, (dangky != null && dangky.NguoiDung.ThanhPhoID == item.Id) ? "selected" : "");
            }
            ViewBag.cbxProvince = cbxProvince;


            string cbxKhoa = "<option value=\"\">Chọn khoa</option>";
            var khoa = db.Khoas.Where(x => x.TrangThaiID == EnumStatus.ACTIVE).OrderByDescending(x => x.Id).ToList();
            foreach (var item in khoa)
            {
                cbxKhoa += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Ten, (dangky != null && dangky.NguoiDung.KhoaID == item.Id) ? "selected" : "");
            }
            ViewBag.cbxKhoa = cbxKhoa;

            string cbxPhongKham = "<option value=\"\">Chọn phòng khám</option>";
            var phongKham = db.Phongs.Where(x => x.TrangThaiID == EnumStatus.ACTIVE && x.LoaiPhong == EnumLoaiPhong.PHONG_KHAM).OrderByDescending(x => x.Id).ToList();
            foreach (var item in phongKham)
            {
                cbxPhongKham += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Ten, (dangky != null && dangky.ChiTietKhamBenh != null && dangky.ChiTietKhamBenh.PhongID == item.Id) ? "selected" : "");
            }
            ViewBag.cbxPhongKham = cbxPhongKham;
            string cbxBenhNhan = "<option value=\"\">Chọn bệnh nhân</option>";
            var benhNhan = db.NguoiDungs.Where(x => x.TrangThaiID == EnumStatus.ACTIVE && x.LoaiNguoiDung == EnumUserType.BENH_NHAN).OrderByDescending(x => x.Id).ToList();
            foreach (var item in benhNhan)
            {
                cbxBenhNhan += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.HoTen, (dangky != null && dangky.NguoiDung.Id == item.Id) ? "selected" : "");
            }
            ViewBag.cbxBenhNhan = cbxBenhNhan;
            return PartialView(dangky);
        }
        [Route("kham-benh/benh-nhan-update")]
        [HttpPost, ValidateInput(false)]
        public ActionResult BenhNhan_Update(KhamBenh khamBenh, NguoiDung user, HttpPostedFileBase _Avatar = null, string NgaySinh = "", string NgayApDungBHYT = "", string NgayHetHanBHYT = "", int? BacSiID = null , int? PhongID = null)
        {
            try
            {
                UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
                if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                    return RedirectToAction("Index", "Home", new { area = "" });
                if (!user.Email.checkEmail())
                    return Json(new { kq = "err", msg = "Email không đúng !" }, JsonRequestBehavior.AllowGet);
                if (!user.SDT.checkSoDienThoai())
                    return Json(new { kq = "err", msg = "Số điện thoại không hợp lệ !" }, JsonRequestBehavior.AllowGet);

                if (khamBenh.ApDungBHYT == EnumStatus.ACTIVE)
                {
                    if (string.IsNullOrEmpty(user.SoBHYT))
                        return Json(new { kq = "err", msg = "Không thể áp dụng BYHT vì chưa nhập số BHYT" }, JsonRequestBehavior.AllowGet);
                    if (string.IsNullOrEmpty(NgayApDungBHYT))
                        return Json(new { kq = "err", msg = "Không thể áp dụng BYHT. Ngày áp dụng thẻ BHYT không chính xác" }, JsonRequestBehavior.AllowGet);
                    if (string.IsNullOrEmpty(NgayHetHanBHYT))
                        return Json(new { kq = "err", msg = "Không thể áp dụng BYHT. Ngày hết hạn thẻ BHYT không chính xác" }, JsonRequestBehavior.AllowGet);
                }
                DateTime? dateOfBirth = NgaySinh.ToDateTime();
                DateTime? ngayApDung = NgayApDungBHYT.ToDateTime();
                DateTime? ngayHetHan = NgayHetHanBHYT.ToDateTime();
                var appConfigs = db.AppConfigs.FirstOrDefault();
                int userID = 0;
                if (khamBenh.NguoiDungID == null)
                {
                    // chua co user thy them vao
                    if (user.Id == 0)
                    {
                        if (_Avatar != null)
                        {
                            string rootPathImage = string.Format("~/Files/nguoi-dung/avatar/{0}", DateTime.Now.ToString("ddMMyyyy"));
                            string filePathImage = System.IO.Path.Combine(Request.MapPath(rootPathImage));
                            string[] fileImage = _Avatar.uploadFile(rootPathImage, filePathImage);
                            user.Avatar = fileImage[1];
                        }
                        user.Code = Functions.CreateCode();
                        user.NgayApDungBHYT = ngayApDung;
                        user.NgayHetHanBHYT = ngayHetHan;
                        user.NgaySinh = dateOfBirth;
                        user.LoaiNguoiDung = EnumUserType.BENH_NHAN;
                        using (var context = new BenhVienEntities())
                        {
                            context.NguoiDungs.Add(user);
                            context.SaveChanges();
                            userID = user.Id;
                        }
                    }                 
                }
                else
                {
                    var userOld = db.NguoiDungs.FirstOrDefault(x => x.Id == khamBenh.NguoiDungID);
                    userID = khamBenh.NguoiDungID.Value;
                    userOld.TLMienGiam = user.TLMienGiam;
                    userOld.Mota = user.Mota;
                }
                int id = 0;
                if (khamBenh.Id == 0)
                {
                    khamBenh.MaSoKhamBenh = Functions.CreateCode6Char();
                    khamBenh.NgayKham = DateTime.Now;
                    khamBenh.NguoiDungID = userID;
                    
                    using(var context = new BenhVienEntities())
                    {
                        context.KhamBenhs.Add(khamBenh);
                        context.SaveChanges();
                        id = khamBenh.Id;
                    }
                    var phong = db.Phongs.FirstOrDefault(x => x.Id == PhongID);
                    ChiTietKhamBenh chitiet = new ChiTietKhamBenh();
                    chitiet.KhamBenhID = id;
                    chitiet.BacSiID = BacSiID;
                    chitiet.PhongID = PhongID;
                    if(phong != null)
                    {
                        chitiet.KhoaID = phong.KhoaID;
                    }
                   
                    db.ChiTietKhamBenhs.Add(chitiet);
                    db.SaveChanges();
                    try
                    {
                        if(khamBenh.TrangThaiID == EnumTrangThaiKhamBenh.DANG_KY)
                        {
                            ExportExcel(id);
                        }
                       
                    }
                    catch
                    {

                    }
                }
                else
                {
                    var old = db.KhamBenhs.FirstOrDefault(x => x.Id == khamBenh.Id);
                    
                    if (old == null)
                        return Json(new { kq = "err", msg = "Thông tin không xác định!" }, JsonRequestBehavior.AllowGet);
                    //old.TrangThaiID = khamBenh.TrangThaiID;
                    id = old.Id;
                    old.ChiDanBacSi = khamBenh.ChiDanBacSi;
                    old.ApDungBHYT = khamBenh.ApDungBHYT;
                    old.KieuKhamBenh = khamBenh.KieuKhamBenh;

                    var chitietTemp = db.ChiTietKhamBenhs.FirstOrDefault(x => x.KhamBenhID == old.Id);
                    if(chitietTemp == null)
                    {
                        ChiTietKhamBenh chitiet = new ChiTietKhamBenh();
                        chitiet.KhamBenhID = id;
                        chitiet.BacSiID = BacSiID;
                        chitiet.PhongID = PhongID;
                        chitiet.KhoaID = user.KhoaID;
                        db.ChiTietKhamBenhs.Add(chitiet);
                    }
                    db.SaveChanges();
                    try
                    {
                        if (khamBenh.TrangThaiID == EnumTrangThaiKhamBenh.DANG_KY)
                        {
                            ExportExcel(old.Id);
                        }

                    }
                    catch
                    {

                    }
                }
                
                return Json(new { kq = "ok", msg = "Thành công!" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { kq = "err", msg = "Lỗi phát sinh yêu cầu" }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("kham-benh/update")]
        public ActionResult Update(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            var dangky = (from a in db.KhamBenhs.ToList()
                          join b in db.NguoiDungs on a.NguoiDungID equals b.Id into b1
                          from b in b1.DefaultIfEmpty()
                          join c in db.ChiTietKhamBenhs on a.Id equals c.KhamBenhID into c1
                          from c in c1.DefaultIfEmpty()
                          join d in db.SuDungDichVus on a.Id equals d.KhamBenhID into d1
                          from d in d1.DefaultIfEmpty()
                          join e in db.DonThuocs on a.Id equals e.KhamBenhID into e1
                          from e in e1.DefaultIfEmpty()
                          where a.Id == id
                          select new ThongTinKhamBenh
                          {
                              NguoiDung = b,
                              KhamBenh = a,
                              ChiTietKhamBenh = c,
                              DonThuoc = e,
                              DichVu = d
                          }
                     )
                     .OrderByDescending(x => x.KhamBenh.NgayKham).FirstOrDefault();

            string cbxBenh = "<option value=\"\">Chọn bệnh </option>";
            var benh = db.Benhs.Where(x => x.TrangThaiID == EnumStatus.ACTIVE).OrderByDescending(x => x.Id).ToList();
            foreach (var item in benh)
            {
                cbxBenh += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Ten, (dangky != null && dangky.ChiTietKhamBenh != null && dangky.ChiTietKhamBenh.BenhID == item.Id) ? "selected" : "");
            }
            ViewBag.cbxBenh = cbxBenh;
            string cbxKhoa = "<option value=\"\">Chọn khoa </option>";
            var khoa = db.Khoas.Where(x => x.TrangThaiID == EnumStatus.ACTIVE).OrderByDescending(x => x.Id).ToList();
            foreach (var item in khoa)
            {
                cbxKhoa += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Ten, (dangky != null && dangky.ChiTietKhamBenh != null && dangky.ChiTietKhamBenh.KhoaID == item.Id) ? "selected" : "");
            }
            ViewBag.cbxKhoa = cbxKhoa;

            if (dangky != null)
            {
                if (dangky.KhamBenh.TrangThaiID == EnumTrangThaiKhamBenh.DANG_KY)
                {
                    dangky.KhamBenh.TrangThaiID = EnumTrangThaiKhamBenh.DANG_KHAM;
                    db.SaveChanges();
                }

            }

            return PartialView(dangky);
        }
        [Route("kham-benh/update")]
        [HttpPost, ValidateInput(false)]
        public ActionResult Update(ChiTietKhamBenh chitiet, int? TrangThaiID = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            var khambenh = db.KhamBenhs.FirstOrDefault(x => x.Id == chitiet.KhamBenhID);
            if (khambenh == null)
                return Json(new { kq = "err", msg = "Không tìm thấy bệnh án!" }, JsonRequestBehavior.AllowGet);

            if (TrangThaiID == EnumTrangThaiKhamBenh.HOAN_THANH)
            {
                // check xem xet nghiem ok chua
                var dichvu = db.SuDungDichVus.Where(x => x.KhamBenhID == khambenh.Id).ToList();
                foreach (var item in dichvu)
                {
                    var chitietdichvu = db.ChiTietSuDungDichVus.Where(x => x.SuDungDichVuID == item.Id);
                    foreach (var d in chitietdichvu)
                    {
                        if (d.TrangThaiID != EnumTrangThaiDichVu.HOAN_THANH)
                            return Json(new { kq = "err", msg = "Các xét nghiệm đang thực hiện. Không thể hoàn thành khám bệnh" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            if (TrangThaiID == EnumTrangThaiKhamBenh.NOI_TRU)
            {
                // insert nhap vien
                var khambenhTemp = db.KhamBenhs.FirstOrDefault(x => x.Id == chitiet.KhamBenhID);
                var user = db.NguoiDungs.FirstOrDefault(x => x.Id == khambenhTemp.NguoiDungID);
                var nhapVien = new NhapVien();
                if (user == null)
                    return Json(new { kq = "err", msg = "Người dùng không chính xác !" }, JsonRequestBehavior.AllowGet);
                nhapVien.MaBenhAn = Functions.CreateCode();
                nhapVien.NgayNhapVien = DateTime.Now;
                nhapVien.TrangThaiID = EnumTrangThaiNhapVien.DANG_KY;
                nhapVien.NguoiDungID = user.Id;

                nhapVien.KieuNhapVien = khambenhTemp.KieuKhamBenh;
                nhapVien.ApDungBHYT = khambenhTemp.ApDungBHYT;
                nhapVien.TLMienGiam = user.TLMienGiam;
                db.NhapViens.Add(nhapVien);
            }
            khambenh.TrangThaiID = TrangThaiID;
            if (chitiet.Id == 0)
            {

                chitiet.BacSiID = nd_dv.User.Id;
                db.ChiTietKhamBenhs.Add(chitiet);
                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // up date
                var old = db.ChiTietKhamBenhs.FirstOrDefault(x => x.Id == chitiet.Id);
                if (old == null)
                    return Json(new { kq = "err", msg = "Thông tin không xác định!" }, JsonRequestBehavior.AllowGet);
                old.Mach = chitiet.Mach;
                old.NhipTho = chitiet.NhipTho;
                old.ChieuCao = chitiet.ChieuCao;
                old.NhietDo = chitiet.NhietDo;
                old.HuyetAp = chitiet.HuyetAp;
                old.CanNang = chitiet.CanNang;
                old.Creatinin = chitiet.Creatinin;
                old.BMI = chitiet.BMI;
                old.KetQuaBanDau = chitiet.KetQuaBanDau;
                old.KetQuaTuTuyenTruoc = chitiet.KetQuaTuTuyenTruoc;
                old.LyDoChuyenTuyen = chitiet.LyDoChuyenTuyen;
                old.ChuanDoanICD = chitiet.ChuanDoanICD;
                old.ChuanDoanBenh = chitiet.ChuanDoanBenh;
                old.BenhID = chitiet.BenhID;
                old.StatusID = chitiet.StatusID;
                old.BacSiID = chitiet.BacSiID;
                old.YTaID = chitiet.YTaID;
                old.DoThanhThai = chitiet.DoThanhThai;
                old.ChuanDoanNguyenNhan = chitiet.ChuanDoanNguyenNhan;
                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("kham-benh/delete")]
        public ActionResult Delete(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });

            var benh = db.Benhs.FirstOrDefault(x => x.Id == id);
            if (benh == null)
                return Json(new { kq = "err", msg = "Không xác định!" }, JsonRequestBehavior.AllowGet);

            db.Benhs.Remove(benh);
            db.SaveChanges();
            return Json(new { kq = "ok", msg = "Thành công!" }, JsonRequestBehavior.AllowGet);
        }

        [Route("kham-benh/change-status")]
        public ActionResult Change_Status(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });

            var benh = db.Benhs.FirstOrDefault(x => x.Id == id);
            if (benh == null)
                return Json(new { kq = "err", msg = "Không xác định!" }, JsonRequestBehavior.AllowGet);

            if (benh.TrangThaiID == EnumStatus.ACTIVE)
                benh.TrangThaiID = EnumStatus.INACTIVE;
            else
                benh.TrangThaiID = EnumStatus.ACTIVE;
            db.SaveChanges();
            return Json(new { kq = "ok", data = benh.TrangThaiID, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }

        [Route("kham-benh/get-info")]
        public ActionResult GetInfoNguoiDung(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            if (id == null)
                return Json(new { kq = "err", msg = "Người dùng không hợp lệ!" }, JsonRequestBehavior.AllowGet);
            var users = db.NguoiDungs.FirstOrDefault(x => x.Id == id);
            if (users == null)
                return Json(new { kq = "err", msg = "Người dùng không hợp lệ!" }, JsonRequestBehavior.AllowGet);
            return Json(new { kq = "ok", data = users, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }


        [Route("kham-benh/don-thuoc")]
        public ActionResult DonThuocKhamBenh(int? id = null, int? thongTinID = null, int? status = EnumStatus.INACTIVE)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });

            var donThuoc = db.DonThuocs.FirstOrDefault(x => x.Id == id);
            string cbxThuoc = "<option value=\"\">Chọn thuốc</option>";
            var thuoc = db.Thuocs.Where(x => x.TrangThaiID == EnumStatus.ACTIVE).OrderByDescending(x => x.Id).ToList();
            foreach (var item in thuoc)
            {
                cbxThuoc += string.Format("<option value=\"{0}\">{1}</option>", item.Id, item.Ten);
            }

            List<ChiTietDonThuoc> list = new List<ChiTietDonThuoc>();
            if (donThuoc != null)
            {
                list = db.ChiTietDonThuocs.Where(x => x.DonThuocID == donThuoc.Id).ToList();
            }

            string cbxDate = "";
            bool check = true;
            List<DonThuoc> donThuocTemp = new List<DonThuoc>();
            if(donThuoc == null)
            {
                donThuocTemp = db.DonThuocs.Where(x => (x.KhamBenhID == null && x.NhapVienID == thongTinID) || (x.NhapVienID == null && x.KhamBenhID == thongTinID)).OrderByDescending(x => x.Id).ToList();
            }
            else
            {
                donThuocTemp = db.DonThuocs.Where(x => x.Id == donThuoc.Id ).OrderByDescending(x => x.Id).ToList();
            }
            foreach (var item in donThuocTemp)
            {
                if (item.NgayPhatThuoc.Value.ToString("dd/MM/yyyy").Equals(DateTime.Now.ToString("dd/MM/yyyy")))
                {
                    check = false;
                }
                cbxDate += string.Format("<option value=\"{0}\">{1}</option>", item.Id, item.NgayPhatThuoc.Value.ToString("dd/MM/yyyy HH:ss:mm"));

            }
            if (check)
            {
                cbxDate += string.Format("<option value=\"{0}\" selected>{1}</option>", 0, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            }
            ViewBag.cbxDate = cbxDate;
            ViewBag.cbxThuoc = cbxThuoc;
            ViewBag.Thuoc = thuoc;
            ViewBag.ChiTiet = list;
            ViewBag.Status = status;
            return PartialView(donThuoc);
        }
        [Route("kham-benh/chi-tiet-don-thuoc")]
        public ActionResult ChiTietDonThuocDetails(int? nhapvienID = null, int? khambenhID = null,string date ="")
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            var donThuoc = db.DonThuocs.Where(x => (SqlFunctions.DatePart("dd", x.NgayPhatThuoc) + "/" + SqlFunctions.DatePart("mm", x.NgayPhatThuoc) + "/" + SqlFunctions.DatePart("yyyy", x.NgayPhatThuoc)).Equals(date) && ( (nhapvienID == null && khambenhID != null && x.KhamBenhID == khambenhID) || (khambenhID == null && nhapvienID != null && x.NhapVienID == nhapvienID))).FirstOrDefault();
            string cbxThuoc = "<option value=\"\">Chọn thuốc</option>";
            var thuoc = db.Thuocs.Where(x => x.TrangThaiID == EnumStatus.ACTIVE).OrderByDescending(x => x.Id).ToList();
            foreach (var item in thuoc)
            {
                cbxThuoc += string.Format("<option value=\"{0}\">{1}</option>", item.Id, item.Ten);
            }
            ViewBag.cbxThuoc = cbxThuoc;
            List<ChiTietDonThuoc> list = new List<ChiTietDonThuoc>();
            if (donThuoc != null)
            {
                list = db.ChiTietDonThuocs.Where(x => x.DonThuocID == donThuoc.Id).ToList();
            }
            ViewBag.Thuoc = thuoc;
            ViewBag.DonThuoc = donThuoc;
            return PartialView(list);
        }
        [HttpPost]
        [Route("kham-benh/don-thuoc")]
        public ActionResult DonThuocKhamBenh(DonThuoc thuoc, List<ChiTietDonThuoc> list)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            //if(list != null)
            //{
            //    var duplicates = list.GroupBy(x => x.ThuocID).Select(x => x).ToList();
            //    if (list.Count != duplicates.Count)
            //        return Json(new { kq = "err", msg = "Thuốc đã bị trùng vui lòng kiểm tra lại" }, JsonRequestBehavior.AllowGet);

            //}
       

            //if (nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI)
            //    return Json(new { kq = "err", msg = "Chỉ bác sĩ mới tạo được đơn thuốc" }, JsonRequestBehavior.AllowGet);
            int donThuocID = 0;
            if (thuoc.Id == 0)
            {

                thuoc.MaDonThuoc = Functions.CreateCode();
               
                thuoc.TrangThaiID = EnumTrangThaiDonThuoc.CHUA_PHAT;
                thuoc.LoaiDonThuoc = EnumLoaiDonThuoc.THEO_DON;
                thuoc.BacSiID = nd_dv.User.Id;
                using (var context = new BenhVienEntities())
                {
                    context.DonThuocs.Add(thuoc);
                    context.SaveChanges();
                    donThuocID = thuoc.Id;
                }
                list = list == null ? new List<ChiTietDonThuoc>() : list;
                foreach (var item in list)
                {
                    item.DonThuocID = donThuocID;

                }
                if (list.Count > 0)
                {
                    db.ChiTietDonThuocs.AddRange(list);
                }
                else
                {
                    var t = db.DonThuocs.FirstOrDefault(x => x.Id == donThuocID);
                    db.DonThuocs.Remove(t);

                }

                db.SaveChanges();
                try
                {
                    if (thuoc.KhamBenhID != 0 || thuoc.KhamBenhID != null)
                    {
                        //ExportChiPhiThuoc(thuoc.KhamBenhID);
                        ExportDonThuoc(thuoc.KhamBenhID);
                    }

                }
                catch
                {

                }
            }
            else
            {
                donThuocID = thuoc.Id;
                // up date
                var old = db.DonThuocs.FirstOrDefault(x => x.Id == thuoc.Id);
                if (old == null)
                    return Json(new { kq = "err", msg = "Thông tin không xác định!" }, JsonRequestBehavior.AllowGet);
                old.LoaiDonThuoc = EnumLoaiDonThuoc.THEO_DON;
                old.BacSiID = nd_dv.User.Id;
                var chitiet = db.ChiTietDonThuocs.Where(x => x.DonThuocID == old.Id).ToList();
                if (chitiet.Count > 0)
                    db.ChiTietDonThuocs.RemoveRange(chitiet);
                list = list == null ? new List<ChiTietDonThuoc>() : list;
                foreach (var item in list)
                {
                    item.DonThuocID = old.Id;
                }
                if (list.Count > 0)
                {
                    db.ChiTietDonThuocs.AddRange(list);
                }
                else
                {
                    var t = db.DonThuocs.FirstOrDefault(x => x.Id == donThuocID);
                    db.DonThuocs.Remove(t);

                }
                db.SaveChanges();
                try
                {
                    if (thuoc.KhamBenhID != 0 || thuoc.KhamBenhID != null)
                    {
                        //ExportChiPhiThuoc(thuoc.KhamBenhID);
                        ExportDonThuoc(thuoc.KhamBenhID);
                    }

                }
                catch
                {

                }
            }
            return Json(new { kq = "ok", data = donThuocID, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }

        [Route("kham-benh/can-lam-sang")]
        public ActionResult SuDungDichVu(int? id = null, int? status = EnumStatus.INACTIVE)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            var sudungdichvu = db.SuDungDichVus.FirstOrDefault(x => x.Id == id);
            string cbxDichVu = "<option value=\"\">Chọn dịch vụ</option>";
            var dichvu = db.DichVus.Where(x => x.TrangThaiID == EnumStatus.ACTIVE).OrderByDescending(x => x.Id).ToList();
            foreach (var item in dichvu)
            {
                cbxDichVu += string.Format("<option value=\"{0}\">{1}</option>", item.Id, item.Ten);
            }

            ViewBag.cbxDichVu = cbxDichVu;
            List<ChiTietSuDungDichVu> list = new List<ChiTietSuDungDichVu>();
            if (sudungdichvu != null)
            {
                list = db.ChiTietSuDungDichVus.Where(x => x.SuDungDichVuID == sudungdichvu.Id).ToList();
            }

            ViewBag.cbxDichVu = cbxDichVu;
            ViewBag.DichVu = dichvu;
            ViewBag.ChiTiet = list;
            ViewBag.Status = status;
            return PartialView(sudungdichvu);
        }

        [HttpPost]
        [Route("kham-benh/can-lam-sang")]
        public ActionResult SuDungDichVu(SuDungDichVu dichvu, List<ChiTietSuDungDichVu> list)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            if (list != null)
            {
                var duplicates = list.GroupBy(x => x.DichVuID).Select(x => x).ToList();
                if (list.Count != duplicates.Count)
                    return Json(new { kq = "err", msg = "Dịch vụ đã bị trùng vui lòng kiểm tra lại" }, JsonRequestBehavior.AllowGet);

                var checkTotal = list.Where(x => x.SoLuong == null);
                if(checkTotal != null)
                {
                    return Json(new { kq = "err", msg = "Dịch vụ chưa có số lượng cần nhập" }, JsonRequestBehavior.AllowGet);
                }

            }
                
            if (dichvu.Id == 0)
            {
                int dichVuID = 0;
                dichvu.MaPhieu = Functions.CreateCode();
                dichvu.NgayTao = DateTime.Now;
                dichvu.BacSiID = nd_dv.User.Id;
                dichvu.TrangThaiID = EnumTrangThaiDichVu.CHUA_THUC_HIEN;
                using (var context = new BenhVienEntities())
                {
                    context.SuDungDichVus.Add(dichvu);
                    context.SaveChanges();
                    dichVuID = dichvu.Id;
                }
                list = list == null ? new List<ChiTietSuDungDichVu>() : list;
                foreach (var item in list)
                {
                    item.SuDungDichVuID = dichVuID;
                    item.NgayTao = DateTime.Now;
                    item.TrangThaiID = EnumTrangThaiDichVu.CHUA_THUC_HIEN;
                }
                if (list.Count > 0)
                {
                    db.ChiTietSuDungDichVus.AddRange(list);
                }
                else
                {
                    var t = db.SuDungDichVus.FirstOrDefault(x => x.Id == dichVuID);
                    db.SuDungDichVus.Remove(t);

                }
               
                db.SaveChanges();
                try
                {
                    if (dichvu.KhamBenhID != null && dichvu.KhamBenhID != 0)
                    {
                        ExportDichVu(dichvu.KhamBenhID);
                    }

                }
                catch
                {

                }
            }
            else
            {
                // up date
                var old = db.SuDungDichVus.FirstOrDefault(x => x.Id == dichvu.Id);
                if (old == null)
                    return Json(new { kq = "err", msg = "Thông tin không xác định!" }, JsonRequestBehavior.AllowGet);

                var chitiet = db.ChiTietSuDungDichVus.Where(x => x.SuDungDichVuID == old.Id).ToList();
                if (chitiet.Count > 0)
                    db.ChiTietSuDungDichVus.RemoveRange(chitiet);
                old.BacSiID = nd_dv.User.Id;
                old.TrangThaiID = EnumTrangThaiDichVu.CHUA_THUC_HIEN;
                list = list == null ? new List<ChiTietSuDungDichVu>() : list;
                foreach (var item in list)
                {
                    item.SuDungDichVuID = old.Id;
                    item.NgayTao = DateTime.Now;
                    //item.TrangThaiID = EnumTrangThaiDichVu.CHUA_THUC_HIEN;
                }
                var listCheck = list.Where(x => x.TrangThaiID == EnumTrangThaiDichVu.HOAN_THANH).ToList();
                if (listCheck.Count == list.Count)
                {
                    old.TrangThaiID = EnumTrangThaiDichVu.HOAN_THANH;
                }
                else
                {
                    old.TrangThaiID = EnumTrangThaiDichVu.DANG_THUC_HIEN;
                }
                if (list.Count > 0)
                {
                    db.ChiTietSuDungDichVus.AddRange(list);
                }
                else
                {
                    var t = db.SuDungDichVus.FirstOrDefault(x => x.Id == old.Id);
                    db.SuDungDichVus.Remove(t);

                }
                db.SaveChanges();
                try
                {
                    if(dichvu.KhamBenhID != null && dichvu.KhamBenhID != 0)
                    {
                        ExportDichVu(dichvu.KhamBenhID);
                    }
                    
                }catch
                {

                }
            }
            return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }

        [Route("inventory/export-excel")]
        public ActionResult ExportExcel(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            Application xlApp = new Application();
            if (xlApp == null)
            {
                return Json(new { kq = "err", msg = "Lỗi không thể sử dụng được thư viện EXCEL" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                var config = db.AppConfigs.FirstOrDefault();
                var data = (from a in db.KhamBenhs.ToList()
                            join b in db.NguoiDungs on a.NguoiDungID equals b.Id into b1
                            from b in b1.DefaultIfEmpty()
                            join c in db.ChiTietKhamBenhs on a.Id equals c.KhamBenhID into c1
                            from c in c1.DefaultIfEmpty()
                            join d in db.SuDungDichVus on a.Id equals d.KhamBenhID into d1
                            from d in d1.DefaultIfEmpty()
                            join e in db.DonThuocs on a.Id equals e.KhamBenhID into e1
                            from e in e1.DefaultIfEmpty()
                            where a.Id == id
                            select new ThongTinKhamBenh
                            {
                                NguoiDung = b,
                                KhamBenh = a,
                                ChiTietKhamBenh = c,
                                DonThuoc = e,
                                DichVu = d
                            }
                    )
                    .OrderByDescending(x => x.KhamBenh.NgayKham).FirstOrDefault();

                if (data == null)
                {
                    return Json(new { kq = "err", msg = "Lỗi không có hóa đơn nào" }, JsonRequestBehavior.AllowGet);
                }

                string fileName = "\\Export\\HDKB_" + DateTime.Now.ToString("ddMMyyyy") + "_" + data.KhamBenh.MaSoKhamBenh + ".xls";
                string filePath = HttpContext.Server.MapPath("~" + fileName);

                xlApp.DisplayAlerts = false;
                xlApp.Visible = false;
                object missing = Type.Missing;
                Workbook wb = xlApp.Workbooks.Add(missing);
                Worksheet ws = (Worksheet)wb.Worksheets[1];

                int fontSizeTieuDe = 18;
                int fontSizeTenTruong = 14;
                int fontSizeNoiDung = 12;
                //Info
                ws.AddValue("A1", "C1", "TRUNG TÂM Y TẾ ABC", fontSizeTieuDe, true, XlHAlign.xlHAlignCenter, true, 20);
                ws.AddValue("A2", "C2", "BỆNH VIỆN ĐA KHOA ", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true);
                //ws.AddValue("A3", "C3", "TRẠM Y TẾ", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false);


                ws.AddValue("F1", "G1", "Mẫu số 03/TYT", fontSizeNoiDung, true, XlHAlign.xlHAlignCenter, false);
                ws.AddValue("F2", "G2", data.KhamBenh.STT, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false);
                ws.AddValue("F3", "G3", "Mã số khám bệnh :" + data.KhamBenh.MaSoKhamBenh, fontSizeNoiDung, true, XlHAlign.xlHAlignCenter, false);


                ws.AddValue("B5", "G5", "PHIẾU ĐĂNG KÍ KHÁM BỆNH", fontSizeTieuDe, true, XlHAlign.xlHAlignCenter, false, 20);
                ws.AddValue("C6", "G6", "KIÊM PHIẾU THU TIỀN KHÁM BỆNH", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);


                ws.AddValue("A7", "B7", "I. Thông tin bệnh nhân", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("D7", "D7", "Mức hưởng : " + data.NguoiDung.TLMienGiam + "%", fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);


                ws.AddValue("A8", "D8", "(1) Họ tên người bệnh :      " + data.NguoiDung.HoTen.ToUpper(), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("E8", "F8", "Ngày sinh :" + data.NguoiDung.NgaySinh.Value.ToString("dd/MM/yyyy"), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 15);
                ws.AddValue("G8", "G8", "Giới tính:" + EnumGioiTinh.ToString(data.NguoiDung.GioiTinh), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);



                ws.AddValue("A9", "D9", "(2) Địa chỉ : " + data.NguoiDung.DiaChi, fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);

                ws.AddValue("A10", "C10", "(3) Số BHYT: " + (data.NguoiDung.SoBHYT != null ? data.NguoiDung.SoBHYT : ""), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("D10", "E10", "Áp dụng từ : " + (data.NguoiDung.SoBHYT != null ? data.NguoiDung.NgayApDungBHYT.Value.ToString("dd/MM/yyyy") : ""), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("F10", "G10", " đến : " + (data.NguoiDung.SoBHYT != null ? data.NguoiDung.NgayHetHanBHYT.Value.ToString("dd/MM/yyyy") : ""), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);

                ws.AddValue("A11", "G11", "(4) Thời gian đến khám : " + data.KhamBenh.NgayKham.Value.Hour + " giờ " + data.KhamBenh.NgayKham.Value.Minute + " phút " + data.KhamBenh.NgayKham.Value.Second + " giây ", fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);

                ws.AddValue("A12", "E12", "(5) Kiểu khám : " + EnumKieuKhamBenh.ToString(data.KhamBenh.KieuKhamBenh), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                string chuanDoan = "";
                string ICD = "";
                if (data.ChiTietKhamBenh != null)
                {
                    if (!string.IsNullOrEmpty(data.ChiTietKhamBenh.ChuanDoanBenh))
                    {
                        chuanDoan = data.ChiTietKhamBenh.ChuanDoanBenh;
                    }
                    if (!string.IsNullOrEmpty(data.ChiTietKhamBenh.ChuanDoanICD))
                    {
                        ICD = data.ChiTietKhamBenh.ChuanDoanICD;
                    }
                }
                ws.AddValue("A13", "E13", "(6) Chuẩn đoán  : " + chuanDoan, fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("F13", "F13", "(6) Mã bệnh  : " + ICD, fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("A14", "G14", "(7) Tình trạng ban đầu : " + (data.NguoiDung != null && !string.IsNullOrEmpty(data.NguoiDung.Mota) ? data.NguoiDung.Mota : ""), fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);

                string khoa = "";
                if(data.NguoiDung != null && data.NguoiDung.KhoaID != null)
                {
                    var k = db.Khoas.FirstOrDefault(x => x.Id == data.NguoiDung.KhoaID);
                    if(k != null)
                    {
                        khoa = k.Ten;
                    }
                }

                ws.AddValue("A15", "G15", "(8) Nơi tiếp nhận : " + khoa, fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("A16", "C16", "II.Chi phí khám chữa bệnh", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);


                int rowStart = 17, rowIndex = 18;
                //Header
                ws.AddValue("A" + rowStart, "A" + rowIndex, "Nội dung", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("B" + rowStart, "B" + rowIndex, "ĐVT", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("C" + rowStart, "C" + rowIndex, "Số lượng", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 10);
                ws.AddValue("D" + rowStart, "D" + rowIndex, "Đơn giá", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 10);
                ws.AddValue("E" + rowStart, "E" + rowIndex, "Thành tiền", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("F" + rowStart, "G" + rowStart, "Nguồn thanh toán", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("F" + rowIndex, "F" + rowIndex, "BHYT", fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("G" + rowIndex, "G" + rowIndex, "Người bệnh", fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, false, 12);




                ws.get_Range("A" + rowStart, "G" + rowIndex).SetBorderAround();


                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "G" + rowIndex, "1. Khám bệnh", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "A" + rowIndex, "Công khám", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("B" + rowIndex, "B" + rowIndex, "lần", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("C" + rowIndex, "C" + rowIndex, "1", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 10);
                ws.AddValue("D" + rowIndex, "D" + rowIndex, config.ChiPhiKhamBenh.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 10);
                ws.AddValue("E" + rowIndex, "E" + rowIndex, config.ChiPhiKhamBenh.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("F" + rowIndex, "F" + rowIndex, (config.ChiPhiKhamBenh.Value * data.NguoiDung.TLMienGiam/100).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("G" + rowIndex, "G" + rowIndex, (config.ChiPhiKhamBenh.Value - (config.ChiPhiKhamBenh.Value * data.NguoiDung.TLMienGiam / 100)).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "A" + rowIndex, "Tổng cộng (1)", fontSizeTenTruong, true, XlHAlign.xlHAlignRight, true, 12);

                ws.AddValue("E" + rowIndex, "E" + rowIndex, config.ChiPhiKhamBenh.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("F" + rowIndex, "F" + rowIndex, (config.ChiPhiKhamBenh.Value * data.NguoiDung.TLMienGiam/100).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("G" + rowIndex, "G" + rowIndex, (config.ChiPhiKhamBenh.Value - (config.ChiPhiKhamBenh.Value * data.NguoiDung.TLMienGiam / 100)).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                //ws.AddValue("A" + rowIndex, "G" + rowIndex, "2. Đơn thuốc", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //rowIndex += 1;
                //var donThuoc = (from a in db.ChiTietDonThuocs.ToList()
                //                join b in db.Thuocs on a.ThuocID equals b.Id into b1
                //                from b in b1.DefaultIfEmpty()
                //                where data.DonThuoc == null ? false : a.DonThuocID == data.DonThuoc.Id
                //                select new
                //                {
                //                    Thuoc = b,
                //                    ChiTiet = a
                //                }).ToList();
                //decimal? tongTienThuoc = 0;
                //double? tongTienThuocBHYT = 0;
                //foreach (var item in donThuoc)
                //{
                //    //tongTienThuoc += item.Thuoc.Gia * item.ChiTiet.SoLuong;
                //    //tongTienThuocBHYT += Convert.ToDouble(item.Thuoc.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam/100;
                //    //double? tongThuoc = Convert.ToDouble(item.Thuoc.Gia * item.ChiTiet.SoLuong);
                //    //double? tongThuocBHYT = Convert.ToDouble(item.Thuoc.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam/100;
                //    tongTienThuoc += item.Thuoc.Gia * item.ChiTiet.SoLuong;
                //    double? tongThuoc = Convert.ToDouble(item.Thuoc.Gia * item.ChiTiet.SoLuong);
                //    double? tongThuocBHYT = 0;
                //    if (item.Thuoc.GiamGiaBHYT == EnumStatus.ACTIVE)
                //    {
                //        tongTienThuocBHYT += Convert.ToDouble(item.Thuoc.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                //        tongThuocBHYT = Convert.ToDouble(item.Thuoc.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                //    }
                //    dynamic[] val = { item.Thuoc.Ten, item.Thuoc.DonVi, item.ChiTiet.SoLuong.Value, item.Thuoc.Gia.Value.ToString("#,###"), tongThuoc.Value.ToString("#,###"), tongThuocBHYT.Value.ToString("#,###"), (Convert.ToDouble(tongThuoc) - tongThuocBHYT).Value.ToString("#,###") };
                //    ws.AddValue("A" + rowIndex, "G" + rowIndex, val, fontSizeNoiDung, false, XlHAlign.xlHAlignLeft, false);
                //    rowIndex += 1;
                //}
                //ws.AddValue("A" + rowIndex, "A" + rowIndex, "Tổng cộng (2)", fontSizeTenTruong, true, XlHAlign.xlHAlignRight, true, 12);
                //ws.AddValue("E" + rowIndex, "E" + rowIndex, tongTienThuoc.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("F" + rowIndex, "F" + rowIndex, (tongTienThuocBHYT.Value * data.NguoiDung.TLMienGiam).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("G" + rowIndex, "G" + rowIndex, (tongTienThuoc - Convert.ToDecimal(tongTienThuocBHYT.Value * data.NguoiDung.TLMienGiam / 100)).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //rowIndex += 1;
                //ws.AddValue("A" + rowIndex, "G" + rowIndex, "3. Xét nghiệm cận lâm sàng (3)", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //rowIndex += 1;

                //var canlamsang = (from a in db.ChiTietSuDungDichVus.ToList()
                //                  join b in db.SuDungDichVus on a.SuDungDichVuID equals b.Id into b1
                //                  from b in b1.DefaultIfEmpty()
                //                  join c in db.DichVus on a.DichVuID equals c.Id
                //                  where b.KhamBenhID == data.KhamBenh.Id
                //                  select new
                //                  {
                //                      DichVu = c,
                //                      SuDungDichVus = b,
                //                      ChiTiet = a
                //                  }).ToList();
                //decimal? tongTienDichVu = 0;
                //double? tongTienDichVuBHYT = 0;
                //foreach (var item in canlamsang)
                //{
                //    //tongTienDichVu += item.DichVu.Gia * item.ChiTiet.SoLuong;
                //    //tongTienDichVuBHYT += Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam/100;
                //    //double? tongDichVu = Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong);
                //    //double? tongDichVuBHYT = Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam/100;

                //    tongTienDichVu += item.DichVu.Gia * item.ChiTiet.SoLuong;
                //    double? tongDichVu = Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong);
                //    double? tongDichVuBHYT = 0;
                //    if (item.DichVu.ApDungBHYT == EnumStatus.ACTIVE)
                //    {
                //        tongTienDichVuBHYT += Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                //        tongDichVuBHYT = Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                //    }

                //    dynamic[] val = { item.DichVu.Ten, "lần", item.ChiTiet.SoLuong.Value, item.DichVu.Gia.Value.ToString("#,###"), tongDichVu.Value.ToString("#,###"), tongDichVuBHYT.Value.ToString("#,###"), (Convert.ToDouble(tongDichVu) - tongDichVuBHYT).Value.ToString("#,###") };
                //    ws.AddValue("A" + rowIndex, "G" + rowIndex, val, fontSizeNoiDung, false, XlHAlign.xlHAlignLeft, false);
                //    rowIndex += 1;
                //}
                //ws.AddValue("A" + rowIndex, "A" + rowIndex, "Tổng cộng (3)", fontSizeTenTruong, true, XlHAlign.xlHAlignRight, true, 12);
                //ws.AddValue("E" + rowIndex, "E" + rowIndex, tongTienDichVu.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("F" + rowIndex, "F" + rowIndex, (tongTienDichVuBHYT.Value * data.NguoiDung.TLMienGiam/100).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("G" + rowIndex, "G" + rowIndex, (tongTienDichVu - Convert.ToDecimal(tongTienDichVuBHYT.Value * data.NguoiDung.TLMienGiam / 100)).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //rowIndex += 1;

                ws.get_Range("A" + rowStart, "G" + rowIndex).SetBorderAround();

                // total
                var total = Convert.ToDecimal(config.ChiPhiKhamBenh);
                var totalBHYT = Convert.ToDouble(config.ChiPhiKhamBenh * data.NguoiDung.TLMienGiam/100);
                ws.AddValue("A" + rowIndex, "C" + rowIndex, "Tổng số tiền (1) (2) (3) :", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("D" + rowIndex, "E" + rowIndex,total.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "C" + rowIndex, "BHYT chi trả (1) (2) (3) :", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("D" + rowIndex, "E" + rowIndex, totalBHYT.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "C" + rowIndex, "Người bệnh chi trả (1) (2) (3) :", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("D" + rowIndex, "E" + rowIndex, (Convert.ToDouble(total) - totalBHYT).ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);

                // SIGN
                rowIndex += 1;
                ws.AddValue("E" + rowIndex, "G" + rowIndex, "Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "B" + rowIndex, "NGƯỜI LẬP", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("C" + rowIndex, "D" + rowIndex, "NGƯỜI BỆNH", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("F" + rowIndex, "G" + rowIndex, "KẾ TOÁN", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "B" + rowIndex, "( ký ghi rõ họ tên )", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("C" + rowIndex, "D" + rowIndex, "( ký ghi rõ họ tên )", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("F" + rowIndex, "G" + rowIndex, "(ký ghi rõ họ tên)", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);

                string fileName1 = "\\Export\\HD" + DateTime.Now.ToString("ddMMyyyy") + data.KhamBenh.MaSoKhamBenh + ".pdf";
                string filePath1 = HttpContext.Server.MapPath("~" + fileName1);


                //Save
                wb.SaveAs(filePath);
                wb.SaveAs(filePath, XlFileFormat.xlOpenXMLWorkbook, missing, missing, false, false
                    , XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlUserResolution
                    , true, missing, missing, missing);
                wb.Saved = true;
                wb.Close(true, missing, missing);
                ExportWorkbookToPdf(filePath, filePath1);
                //wb.Close();
                //thoát và thu hồi bộ nhớ cho COM
                ws.ReleaseObject();
                wb.ReleaseObject();
 
                return Json(new { kq = "ok", data = filePath1, msg = "Thành công!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { kq = "err", msg = "Đã xảy ra lỗi khi lưu dữ liệu. Kiểm tra File" }, JsonRequestBehavior.AllowGet);
            }
            finally
            {
                if (xlApp != null)
                {
                    xlApp.Quit();
                }
                xlApp.ReleaseObject();
            }
        }


        [Route("inventory/convert-export-excel")]
        public bool ExportWorkbookToPdf(string workbookPath, string outputPath)
        {
            // If either required string is null or empty, stop and bail out
            if (string.IsNullOrEmpty(workbookPath) || string.IsNullOrEmpty(outputPath))
            {
                return false;
            }

            // Create COM Objects
            Microsoft.Office.Interop.Excel.Application excelApplication;
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook;

            // Create new instance of Excel
            excelApplication = new Microsoft.Office.Interop.Excel.Application();

            // Make the process invisible to the user
            excelApplication.ScreenUpdating = false;

            // Make the process silent
            excelApplication.DisplayAlerts = false;

            // Open the workbook that you wish to export to PDF
            excelWorkbook = excelApplication.Workbooks.Open(workbookPath);

            // If the workbook failed to open, stop, clean up, and bail out
            if (excelWorkbook == null)
            {
                excelApplication.Quit();

                excelApplication = null;
                excelWorkbook = null;

                return false;
            }

            var exportSuccessful = true;
            try
            {
                // Call Excel's native export function (valid in Office 2007 and Office 2010, AFAIK)
                excelWorkbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, outputPath);
            }
            catch (System.Exception)
            {
                // Mark the export as failed for the return value...
                exportSuccessful = false;

                // Do something with any exceptions here, if you wish...
                // MessageBox.Show...        
            }
            finally
            {
                // Close the workbook, quit the Excel, and clean up regardless of the results...
                excelWorkbook.Close();
                excelApplication.Quit();

                excelApplication = null;
                excelWorkbook = null;
            }

            // You can use the following method to automatically open the PDF after export if you wish
            // Make sure that the file actually exists first...
            if (System.IO.File.Exists(outputPath))
            {
                System.Diagnostics.Process.Start(outputPath);
            }

            return exportSuccessful;
        }


        [Route("charge/export-excel-charge-service")]
        public ActionResult ExportDichVu(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            Application xlApp = new Application();
            if (xlApp == null)
            {
                return Json(new { kq = "err", msg = "Lỗi không thể sử dụng được thư viện EXCEL" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                var config = db.AppConfigs.FirstOrDefault();
                var data = (from a in db.KhamBenhs.ToList()
                            join b in db.NguoiDungs on a.NguoiDungID equals b.Id into b1
                            from b in b1.DefaultIfEmpty()
                            join c in db.ChiTietKhamBenhs on a.Id equals c.KhamBenhID into c1
                            from c in c1.DefaultIfEmpty()
                            join d in db.SuDungDichVus on a.Id equals d.KhamBenhID into d1
                            from d in d1.DefaultIfEmpty()
                            join e in db.DonThuocs on a.Id equals e.KhamBenhID into e1
                            from e in e1.DefaultIfEmpty()
                            where a.Id == id
                            select new ThongTinKhamBenh
                            {
                                NguoiDung = b,
                                KhamBenh = a,
                                ChiTietKhamBenh = c,
                                DonThuoc = e,
                                DichVu = d
                            }
                    )
                    .OrderByDescending(x => x.KhamBenh.NgayKham).FirstOrDefault();

                if (data == null)
                {
                    return Json(new { kq = "err", msg = "Lỗi không có hóa đơn nào" }, JsonRequestBehavior.AllowGet);
                }

                string fileName = "\\Export\\DichVu" + DateTime.Now.ToString("ddMMyyyy") + "_" + data.KhamBenh.MaSoKhamBenh + ".xls";
                string filePath = HttpContext.Server.MapPath("~" + fileName);

                xlApp.DisplayAlerts = false;
                xlApp.Visible = false;
                object missing = Type.Missing;
                Workbook wb = xlApp.Workbooks.Add(missing);
                Worksheet ws = (Worksheet)wb.Worksheets[1];

                int fontSizeTieuDe = 18;
                int fontSizeTenTruong = 14;
                int fontSizeNoiDung = 12;
                //Info
                ws.AddValue("A1", "C1", "TRUNG TÂM Y TẾ ABC", fontSizeTieuDe, true, XlHAlign.xlHAlignCenter, true, 20);
                ws.AddValue("A2", "C2", "BỆNH VIỆN ĐA KHOA ", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true);
                //ws.AddValue("A3", "C3", "TRẠM Y TẾ", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false);


                ws.AddValue("F1", "G1", "Mẫu số 03/TYT", fontSizeNoiDung, true, XlHAlign.xlHAlignCenter, false);
                ws.AddValue("F2", "G2", data.KhamBenh.STT, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false);
                ws.AddValue("F3", "G3", "Mã số khám bệnh :" + data.KhamBenh.MaSoKhamBenh, fontSizeNoiDung, true, XlHAlign.xlHAlignCenter, false);




                ws.AddValue("B5", "G5", "PHIẾU CHỈ ĐỊNH DỊCH VỤ CẬN LÂM SÀNG", fontSizeTieuDe, true, XlHAlign.xlHAlignCenter, false, 20);
                //ws.AddValue("D6", "E6", "TẠI TRẠM Y TẾ", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);


                ws.AddValue("A7", "B7", "I. Thông tin bệnh nhân", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("D7", "D7", "Mức hưởng : " + data.NguoiDung.TLMienGiam + "%", fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);


                ws.AddValue("A8", "D8", "(1) Họ tên người bệnh :      " + data.NguoiDung.HoTen.ToUpper(), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("E8", "F8", "Ngày sinh :" + data.NguoiDung.NgaySinh.Value.ToString("dd/MM/yyyy"), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 15);
                ws.AddValue("G8", "G8", "Giới tính:" + EnumGioiTinh.ToString(data.NguoiDung.GioiTinh), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);



                ws.AddValue("A9", "D9", "(2) Địa chỉ : " + data.NguoiDung.DiaChi, fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);

                ws.AddValue("A10", "C10", "(3) Số BHYT: " + (data.NguoiDung.SoBHYT != null ? data.NguoiDung.SoBHYT : ""), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("D10", "E10", "Áp dụng từ : " + (data.NguoiDung.SoBHYT != null ? data.NguoiDung.NgayApDungBHYT.Value.ToString("dd/MM/yyyy") : ""), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("F10", "G10", " đến : " + (data.NguoiDung.SoBHYT != null ? data.NguoiDung.NgayHetHanBHYT.Value.ToString("dd/MM/yyyy") : ""), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);

                ws.AddValue("A11", "G11", "(4) Thời gian đến khám : " + data.KhamBenh.NgayKham.Value.Hour + " giờ " + data.KhamBenh.NgayKham.Value.Minute + " phút " + data.KhamBenh.NgayKham.Value.Second + " giây ", fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);

                ws.AddValue("A12", "E12", "(5) Kiểu khám : " + EnumKieuKhamBenh.ToString(data.KhamBenh.KieuKhamBenh), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                string chuanDoan = "";
                string ICD = "";
                if (data.ChiTietKhamBenh != null)
                {
                    if (!string.IsNullOrEmpty(data.ChiTietKhamBenh.ChuanDoanBenh))
                    {
                        chuanDoan = data.ChiTietKhamBenh.ChuanDoanBenh;
                    }
                    if (!string.IsNullOrEmpty(data.ChiTietKhamBenh.ChuanDoanICD))
                    {
                        ICD = data.ChiTietKhamBenh.ChuanDoanICD;
                    }
                }
                ws.AddValue("A13", "E13", "(6) Chuẩn đoán  : " + chuanDoan, fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("F13", "F13", "(6) Mã bệnh  : " + ICD, fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);


                ws.AddValue("A14", "C14", "II.Chi phí khám chữa bệnh", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);


                int rowStart = 15, rowIndex = 16;
                //Header
                ws.AddValue("A" + rowStart, "A" + rowIndex, "Nội dung", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("B" + rowStart, "B" + rowIndex, "ĐVT", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("C" + rowStart, "C" + rowIndex, "Số lượng", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 10);
                ws.AddValue("D" + rowStart, "D" + rowIndex, "Đơn giá", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 10);
                ws.AddValue("E" + rowStart, "E" + rowIndex, "Thành tiền", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("F" + rowStart, "G" + rowStart, "Nguồn thanh toán", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("F" + rowIndex, "F" + rowIndex, "BHYT", fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("G" + rowIndex, "G" + rowIndex, "Người bệnh", fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, false, 12);




                ws.get_Range("A" + rowStart, "G" + rowIndex).SetBorderAround();


                //rowIndex += 1;
                //ws.AddValue("A" + rowIndex, "G" + rowIndex, "1. Khám bệnh", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //rowIndex += 1;
                //ws.AddValue("A" + rowIndex, "A" + rowIndex, "Công khám", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("B" + rowIndex, "B" + rowIndex, "lần", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("C" + rowIndex, "C" + rowIndex, "1", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 10);
                //ws.AddValue("D" + rowIndex, "D" + rowIndex, config.ChiPhiKhamBenh.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 10);
                //ws.AddValue("E" + rowIndex, "E" + rowIndex, config.ChiPhiKhamBenh.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("F" + rowIndex, "F" + rowIndex, (config.ChiPhiKhamBenh.Value * data.NguoiDung.TLMienGiam / 100).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("G" + rowIndex, "G" + rowIndex, (config.ChiPhiKhamBenh.Value - (config.ChiPhiKhamBenh.Value * data.NguoiDung.TLMienGiam / 100)).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //rowIndex += 1;
                //ws.AddValue("A" + rowIndex, "A" + rowIndex, "Tổng cộng (1)", fontSizeTenTruong, true, XlHAlign.xlHAlignRight, true, 12);

                //ws.AddValue("E" + rowIndex, "E" + rowIndex, config.ChiPhiKhamBenh.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("F" + rowIndex, "F" + rowIndex, (config.ChiPhiKhamBenh.Value * data.NguoiDung.TLMienGiam / 100).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("G" + rowIndex, "G" + rowIndex, (config.ChiPhiKhamBenh.Value - (config.ChiPhiKhamBenh.Value * data.NguoiDung.TLMienGiam / 100)).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //rowIndex += 1;
                //ws.AddValue("A" + rowIndex, "G" + rowIndex, "2. Đơn thuốc", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //rowIndex += 1;
                //var donThuoc = (from a in db.ChiTietDonThuocs.ToList()
                //                join b in db.Thuocs on a.ThuocID equals b.Id into b1
                //                from b in b1.DefaultIfEmpty()
                //                where data.DonThuoc == null ? false : a.DonThuocID == data.DonThuoc.Id
                //                select new
                //                {
                //                    Thuoc = b,
                //                    ChiTiet = a
                //                }).ToList();
                //decimal? tongTienThuoc = 0;
                //double? tongTienThuocBHYT = 0;
                //foreach (var item in donThuoc)
                //{
                //    tongTienThuoc += item.Thuoc.Gia * item.ChiTiet.SoLuong;
                //    double? tongThuoc = Convert.ToDouble(item.Thuoc.Gia * item.ChiTiet.SoLuong);
                //    double? tongThuocBHYT = 0;
                //    if (item.Thuoc.GiamGiaBHYT == EnumStatus.ACTIVE)
                //    {
                //        tongTienThuocBHYT += Convert.ToDouble(item.Thuoc.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                //        tongThuocBHYT = Convert.ToDouble(item.Thuoc.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                //    }
                //    dynamic[] val = { item.Thuoc.Ten, item.Thuoc.DonVi, item.ChiTiet.SoLuong.Value, item.Thuoc.Gia.Value.ToString("#,###"), tongThuoc.Value.ToString("#,###"), tongThuocBHYT.Value.ToString("#,###"), (Convert.ToDouble(tongThuoc) - tongThuocBHYT).Value.ToString("#,###") };
                //    ws.AddValue("A" + rowIndex, "G" + rowIndex, val, fontSizeNoiDung, false, XlHAlign.xlHAlignLeft, false);
                //    rowIndex += 1;
                //}
                //ws.AddValue("A" + rowIndex, "A" + rowIndex, "Tổng cộng (2)", fontSizeTenTruong, true, XlHAlign.xlHAlignRight, true, 12);
                //ws.AddValue("E" + rowIndex, "E" + rowIndex, tongTienThuoc.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("F" + rowIndex, "F" + rowIndex, (tongTienThuocBHYT.Value * data.NguoiDung.TLMienGiam).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("G" + rowIndex, "G" + rowIndex, (tongTienThuoc - Convert.ToDecimal(tongTienThuocBHYT.Value * data.NguoiDung.TLMienGiam / 100)).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //rowIndex += 1;
                //ws.AddValue("A" + rowIndex, "G" + rowIndex, "3. Xét nghiệm cận lâm sàng (3)", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;

                var canlamsang = (from a in db.ChiTietSuDungDichVus.ToList()
                                  join b in db.SuDungDichVus on a.SuDungDichVuID equals b.Id into b1
                                  from b in b1.DefaultIfEmpty()
                                  join c in db.DichVus on a.DichVuID equals c.Id
                                  where b.KhamBenhID == data.KhamBenh.Id
                                  select new
                                  {
                                      DichVu = c,
                                      SuDungDichVus = b,
                                      ChiTiet = a
                                  }).ToList();
                decimal? tongTienDichVu = 0;
                double? tongTienDichVuBHYT = 0;
                foreach (var item in canlamsang)
                {
                    //tongTienDichVu += item.DichVu.Gia * item.ChiTiet.SoLuong;
                    //tongTienDichVuBHYT += Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam/100;
                    //double? tongDichVu = Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong);
                    //double? tongDichVuBHYT = Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam/100;

                    tongTienDichVu += item.DichVu.Gia * item.ChiTiet.SoLuong;
                    double? tongDichVu = Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong);
                    double? tongDichVuBHYT = 0;
                    if (item.DichVu.ApDungBHYT == EnumStatus.ACTIVE)
                    {
                        tongTienDichVuBHYT += Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                        tongDichVuBHYT = Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                    }

                    dynamic[] val = { item.DichVu.Ten, "lần", item.ChiTiet.SoLuong.Value, item.DichVu.Gia.Value.ToString("#,###"), tongDichVu.Value.ToString("#,###"), tongDichVuBHYT.Value.ToString("#,###"), (Convert.ToDouble(tongDichVu) - tongDichVuBHYT).Value.ToString("#,###") };
                    ws.AddValue("A" + rowIndex, "G" + rowIndex, val, fontSizeNoiDung, false, XlHAlign.xlHAlignLeft, false);
                    rowIndex += 1;
                }
                ws.AddValue("A" + rowIndex, "A" + rowIndex, "Tổng cộng (3)", fontSizeTenTruong, true, XlHAlign.xlHAlignRight, true, 12);
                ws.AddValue("E" + rowIndex, "E" + rowIndex, tongTienDichVu.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("F" + rowIndex, "F" + rowIndex, (tongTienDichVuBHYT).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("G" + rowIndex, "G" + rowIndex, (tongTienDichVu - Convert.ToDecimal(tongTienDichVuBHYT)).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;

                ws.get_Range("A" + rowStart, "G" + rowIndex).SetBorderAround();

                // total
                var total = tongTienDichVu;
                var totalBHYT =  tongTienDichVuBHYT;
                ws.AddValue("A" + rowIndex, "C" + rowIndex, "Tổng số tiền (1) (2) (3) :", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("D" + rowIndex, "E" + rowIndex, (total.Value == 0 || total == null) ? "0" : total.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "C" + rowIndex, "BHYT chi trả (1) (2) (3) :", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("D" + rowIndex, "E" + rowIndex, (totalBHYT.Value == 0 || totalBHYT == null) ? "0" : totalBHYT.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "C" + rowIndex, "Người bệnh chi trả (1) (2) (3) :", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("D" + rowIndex, "E" + rowIndex, (Convert.ToDouble(total) - totalBHYT).Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);

                // SIGN
                rowIndex += 1;
                ws.AddValue("E" + rowIndex, "G" + rowIndex, "Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "B" + rowIndex, "NGƯỜI LẬP", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("C" + rowIndex, "D" + rowIndex, "NGƯỜI BỆNH", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("F" + rowIndex, "G" + rowIndex, "KẾ TOÁN", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "B" + rowIndex, "( ký ghi rõ họ tên )", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("C" + rowIndex, "D" + rowIndex, "( ký ghi rõ họ tên )", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("F" + rowIndex, "G" + rowIndex, "(ký ghi rõ họ tên)", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);

                rowIndex += 1;
                rowIndex += 1;
                rowIndex += 1;
                string tenBacSi = "";
                string tenbenhnhan = "";
                if (data.NguoiDung != null)
                {
                    tenbenhnhan = data.NguoiDung.HoTen;
                }
                if (data.DonThuoc != null)
                {
                    var bacsi = db.NguoiDungs.FirstOrDefault(x => x.Id == data.DonThuoc.BacSiID);

                    if (bacsi != null)
                    {
                        tenBacSi = bacsi.HoTen;
                    }
                }
                ws.AddValue("A" + rowIndex, "B" + rowIndex, tenBacSi, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("C" + rowIndex, "D" + rowIndex, tenbenhnhan, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);

                string fileName1 = "\\Export\\DichVu_HD" + DateTime.Now.ToString("ddMMyyyy") + data.KhamBenh.MaSoKhamBenh + ".pdf";
                string filePath1 = HttpContext.Server.MapPath("~" + fileName1);


                //Save
                wb.SaveAs(filePath);
                wb.SaveAs(filePath, XlFileFormat.xlOpenXMLWorkbook, missing, missing, false, false
                    , XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlUserResolution
                    , true, missing, missing, missing);
                wb.Saved = true;
                wb.Close(true, missing, missing);
                ExportWorkbookToPdf(filePath, filePath1);
                //wb.Close();
                //thoát và thu hồi bộ nhớ cho COM
                ws.ReleaseObject();
                wb.ReleaseObject();

                return Json(new { kq = "ok", data = filePath1, msg = "Thành công!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { kq = "err", msg = "Đã xảy ra lỗi khi lưu dữ liệu. Kiểm tra File" }, JsonRequestBehavior.AllowGet);
            }
            finally
            {
                if (xlApp != null)
                {
                    xlApp.Quit();
                }
                xlApp.ReleaseObject();
            }
        }


        [Route("charge/export-excel-charge-medicine")]
        public ActionResult ExportChiPhiThuoc(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            Application xlApp = new Application();
            if (xlApp == null)
            {
                return Json(new { kq = "err", msg = "Lỗi không thể sử dụng được thư viện EXCEL" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                var config = db.AppConfigs.FirstOrDefault();
                var data = (from a in db.KhamBenhs.ToList()
                            join b in db.NguoiDungs on a.NguoiDungID equals b.Id into b1
                            from b in b1.DefaultIfEmpty()
                            join c in db.ChiTietKhamBenhs on a.Id equals c.KhamBenhID into c1
                            from c in c1.DefaultIfEmpty()
                            join d in db.SuDungDichVus on a.Id equals d.KhamBenhID into d1
                            from d in d1.DefaultIfEmpty()
                            join e in db.DonThuocs on a.Id equals e.KhamBenhID into e1
                            from e in e1.DefaultIfEmpty()
                            where a.Id == id
                            select new ThongTinKhamBenh
                            {
                                NguoiDung = b,
                                KhamBenh = a,
                                ChiTietKhamBenh = c,
                                DonThuoc = e,
                                DichVu = d
                            }
                    )
                    .OrderByDescending(x => x.KhamBenh.NgayKham).FirstOrDefault();

                if (data == null)
                {
                    return Json(new { kq = "err", msg = "Lỗi không có hóa đơn nào" }, JsonRequestBehavior.AllowGet);
                }

                string fileName = "\\Export\\DTBN" + DateTime.Now.ToString("ddMMyyyy") + "_" + data.KhamBenh.MaSoKhamBenh + ".xls";
                string filePath = HttpContext.Server.MapPath("~" + fileName);

                xlApp.DisplayAlerts = false;
                xlApp.Visible = false;
                object missing = Type.Missing;
                Workbook wb = xlApp.Workbooks.Add(missing);
                Worksheet ws = (Worksheet)wb.Worksheets[1];

                int fontSizeTieuDe = 18;
                int fontSizeTenTruong = 14;
                int fontSizeNoiDung = 12;
                //Info
                ws.AddValue("A1", "C1", "TRUNG TÂM Y TẾ ABC", fontSizeTieuDe, true, XlHAlign.xlHAlignCenter, true, 20);
                ws.AddValue("A2", "C2", "BỆNH VIỆN ĐA KHOA ", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true);
                //ws.AddValue("A3", "C3", "TRẠM Y TẾ", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false);


                ws.AddValue("F1", "G1", "Mẫu số 03/TYT", fontSizeNoiDung, true, XlHAlign.xlHAlignCenter, false);
                ws.AddValue("F2", "G2", data.KhamBenh.STT, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false);
                ws.AddValue("F3", "G3", "Mã số khám bệnh :" + data.KhamBenh.MaSoKhamBenh, fontSizeNoiDung, true, XlHAlign.xlHAlignCenter, false);




                ws.AddValue("B5", "G5", "BẢNG KÊ KHAI CHI PHÍ THUỐC", fontSizeTieuDe, true, XlHAlign.xlHAlignCenter, false, 20);
                //ws.AddValue("D6", "E6", "TẠI TRẠM Y TẾ", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);


                ws.AddValue("A7", "B7", "I. Thông tin bệnh nhân", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("D7", "D7", "Mức hưởng : " + data.NguoiDung.TLMienGiam + "%", fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);


                ws.AddValue("A8", "D8", "(1) Họ tên người bệnh :      " + data.NguoiDung.HoTen.ToUpper(), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("E8", "F8", "Ngày sinh :" + data.NguoiDung.NgaySinh.Value.ToString("dd/MM/yyyy"), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 15);
                ws.AddValue("G8", "G8", "Giới tính:" + EnumGioiTinh.ToString(data.NguoiDung.GioiTinh), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);



                ws.AddValue("A9", "D9", "(2) Địa chỉ : " + data.NguoiDung.DiaChi, fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);

                ws.AddValue("A10", "C10", "(3) Số BHYT: " + (data.NguoiDung.SoBHYT != null ? data.NguoiDung.SoBHYT : ""), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("D10", "E10", "Áp dụng từ : " + (data.NguoiDung.SoBHYT != null ? data.NguoiDung.NgayApDungBHYT.Value.ToString("dd/MM/yyyy") : ""), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("F10", "G10", " đến : " + (data.NguoiDung.SoBHYT != null ? data.NguoiDung.NgayHetHanBHYT.Value.ToString("dd/MM/yyyy") : ""), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);

                ws.AddValue("A11", "G11", "(4) Thời gian đến khám : " + data.KhamBenh.NgayKham.Value.Hour + " giờ " + data.KhamBenh.NgayKham.Value.Minute + " phút " + data.KhamBenh.NgayKham.Value.Second + " giây ", fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);

                ws.AddValue("A12", "E12", "(5) Kiểu khám : " + EnumKieuKhamBenh.ToString(data.KhamBenh.KieuKhamBenh), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                string chuanDoan = "";
                string ICD = "";
                if (data.ChiTietKhamBenh != null)
                {
                    if (!string.IsNullOrEmpty(data.ChiTietKhamBenh.ChuanDoanBenh))
                    {
                        chuanDoan = data.ChiTietKhamBenh.ChuanDoanBenh;
                    }
                    if (!string.IsNullOrEmpty(data.ChiTietKhamBenh.ChuanDoanICD))
                    {
                        ICD = data.ChiTietKhamBenh.ChuanDoanICD;
                    }
                }
                ws.AddValue("A13", "E13", "(6) Chuẩn đoán  : " + chuanDoan, fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("F13", "F13", "(6) Mã bệnh  : " + ICD, fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);


                ws.AddValue("A14", "C14", "II.Danh sách thuốc", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);


                int rowStart = 15, rowIndex = 16;
                //Header
                ws.AddValue("A" + rowStart, "A" + rowIndex, "Nội dung", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("B" + rowStart, "B" + rowIndex, "ĐVT", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("C" + rowStart, "C" + rowIndex, "Số lượng", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 10);
                ws.AddValue("D" + rowStart, "D" + rowIndex, "Đơn giá", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 10);
                ws.AddValue("E" + rowStart, "E" + rowIndex, "Thành tiền", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("F" + rowStart, "G" + rowStart, "Nguồn thanh toán", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("F" + rowIndex, "F" + rowIndex, "BHYT", fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("G" + rowIndex, "G" + rowIndex, "Người bệnh", fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, false, 12);




                ws.get_Range("A" + rowStart, "G" + rowIndex).SetBorderAround();


                rowIndex += 1;
                //ws.AddValue("A" + rowIndex, "G" + rowIndex, "1. Khám bệnh", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //rowIndex += 1;
                //ws.AddValue("A" + rowIndex, "A" + rowIndex, "Công khám", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("B" + rowIndex, "B" + rowIndex, "lần", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("C" + rowIndex, "C" + rowIndex, "1", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 10);
                //ws.AddValue("D" + rowIndex, "D" + rowIndex, config.ChiPhiKhamBenh.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 10);
                //ws.AddValue("E" + rowIndex, "E" + rowIndex, config.ChiPhiKhamBenh.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("F" + rowIndex, "F" + rowIndex, (config.ChiPhiKhamBenh.Value * data.NguoiDung.TLMienGiam / 100).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("G" + rowIndex, "G" + rowIndex, (config.ChiPhiKhamBenh.Value - (config.ChiPhiKhamBenh.Value * data.NguoiDung.TLMienGiam / 100)).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //rowIndex += 1;
                //ws.AddValue("A" + rowIndex, "A" + rowIndex, "Tổng cộng (1)", fontSizeTenTruong, true, XlHAlign.xlHAlignRight, true, 12);

                //ws.AddValue("E" + rowIndex, "E" + rowIndex, config.ChiPhiKhamBenh.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("F" + rowIndex, "F" + rowIndex, (config.ChiPhiKhamBenh.Value * data.NguoiDung.TLMienGiam / 100).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("G" + rowIndex, "G" + rowIndex, (config.ChiPhiKhamBenh.Value - (config.ChiPhiKhamBenh.Value * data.NguoiDung.TLMienGiam / 100)).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //rowIndex += 1;
                ws.AddValue("A" + rowIndex, "G" + rowIndex, "1. Đơn thuốc", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                var donThuoc = (from a in db.ChiTietDonThuocs.ToList()
                                join b in db.Thuocs on a.ThuocID equals b.Id into b1
                                from b in b1.DefaultIfEmpty()
                                where data.DonThuoc == null ? false : a.DonThuocID == data.DonThuoc.Id
                                select new
                                {
                                    Thuoc = b,
                                    ChiTiet = a
                                }).ToList();
                decimal? tongTienThuoc = 0;
                double? tongTienThuocBHYT = 0;
                foreach (var item in donThuoc)
                {
                    tongTienThuoc += item.Thuoc.Gia * item.ChiTiet.SoLuong;
                    double? tongThuoc = Convert.ToDouble(item.Thuoc.Gia * item.ChiTiet.SoLuong);
                    double? tongThuocBHYT = 0;
                    if (item.Thuoc.GiamGiaBHYT == EnumStatus.ACTIVE)
                    {
                        tongTienThuocBHYT += Convert.ToDouble(item.Thuoc.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                        tongThuocBHYT = Convert.ToDouble(item.Thuoc.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                    }
                    dynamic[] val = { item.Thuoc.Ten, item.Thuoc.DonVi, item.ChiTiet.SoLuong.Value, item.Thuoc.Gia.Value.ToString("#,###"), tongThuoc.Value.ToString("#,###"), tongThuocBHYT.Value.ToString("#,###"), (Convert.ToDouble(tongThuoc) - tongThuocBHYT).Value.ToString("#,###") };
                    ws.AddValue("A" + rowIndex, "G" + rowIndex, val, fontSizeNoiDung, false, XlHAlign.xlHAlignLeft, false);
                    rowIndex += 1;
                }
                ws.AddValue("A" + rowIndex, "A" + rowIndex, "Tổng cộng (2)", fontSizeTenTruong, true, XlHAlign.xlHAlignRight, true, 12);
                ws.AddValue("E" + rowIndex, "E" + rowIndex, tongTienThuoc.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("F" + rowIndex, "F" + rowIndex, (tongTienThuocBHYT).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("G" + rowIndex, "G" + rowIndex, (tongTienThuoc - Convert.ToDecimal(tongTienThuocBHYT.Value)).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                //ws.AddValue("A" + rowIndex, "G" + rowIndex, "3. Xét nghiệm cận lâm sàng (3)", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //rowIndex += 1;

                //var canlamsang = (from a in db.ChiTietSuDungDichVus.ToList()
                //                  join b in db.SuDungDichVus on a.SuDungDichVuID equals b.Id into b1
                //                  from b in b1.DefaultIfEmpty()
                //                  join c in db.DichVus on a.DichVuID equals c.Id
                //                  where b.KhamBenhID == data.KhamBenh.Id
                //                  select new
                //                  {
                //                      DichVu = c,
                //                      SuDungDichVus = b,
                //                      ChiTiet = a
                //                  }).ToList();
                //decimal? tongTienDichVu = 0;
                //double? tongTienDichVuBHYT = 0;
                //foreach (var item in canlamsang)
                //{
                //    //tongTienDichVu += item.DichVu.Gia * item.ChiTiet.SoLuong;
                //    //tongTienDichVuBHYT += Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam/100;
                //    //double? tongDichVu = Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong);
                //    //double? tongDichVuBHYT = Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam/100;

                //    tongTienDichVu += item.DichVu.Gia * item.ChiTiet.SoLuong;
                //    double? tongDichVu = Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong);
                //    double? tongDichVuBHYT = 0;
                //    if (item.DichVu.ApDungBHYT == EnumStatus.ACTIVE)
                //    {
                //        tongTienDichVuBHYT += Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                //        tongDichVuBHYT = Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                //    }

                //    dynamic[] val = { item.DichVu.Ten, "lần", item.ChiTiet.SoLuong.Value, item.DichVu.Gia.Value.ToString("#,###"), tongDichVu.Value.ToString("#,###"), tongDichVuBHYT.Value.ToString("#,###"), (Convert.ToDouble(tongDichVu) - tongDichVuBHYT).Value.ToString("#,###") };
                //    ws.AddValue("A" + rowIndex, "G" + rowIndex, val, fontSizeNoiDung, false, XlHAlign.xlHAlignLeft, false);
                //    rowIndex += 1;
                //}
                //ws.AddValue("A" + rowIndex, "A" + rowIndex, "Tổng cộng (3)", fontSizeTenTruong, true, XlHAlign.xlHAlignRight, true, 12);
                //ws.AddValue("E" + rowIndex, "E" + rowIndex, tongTienDichVu.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("F" + rowIndex, "F" + rowIndex, (tongTienDichVuBHYT.Value * data.NguoiDung.TLMienGiam / 100).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //ws.AddValue("G" + rowIndex, "G" + rowIndex, (tongTienDichVu - Convert.ToDecimal(tongTienDichVuBHYT.Value * data.NguoiDung.TLMienGiam / 100)).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                //rowIndex += 1;

                ws.get_Range("A" + rowStart, "G" + rowIndex).SetBorderAround();

                // total
                var total = tongTienThuoc;
                var totalBHYT = tongTienThuocBHYT;
                ws.AddValue("A" + rowIndex, "C" + rowIndex, "Tổng số tiền (1) (2) (3) :", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("D" + rowIndex, "E" + rowIndex, (total.Value == 0 || total == null) ? "0" : total.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "C" + rowIndex, "BHYT chi trả (1) (2) (3) :", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("D" + rowIndex, "E" + rowIndex, (totalBHYT.Value == 0 || totalBHYT == null) ? "0" : totalBHYT.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "C" + rowIndex, "Người bệnh chi trả (1) (2) (3) :", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("D" + rowIndex, "E" + rowIndex, (Convert.ToDouble(total) - totalBHYT).Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);

                // SIGN
                rowIndex += 1;
                ws.AddValue("E" + rowIndex, "G" + rowIndex, "Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "B" + rowIndex, "NGƯỜI LẬP", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("C" + rowIndex, "D" + rowIndex, "NGƯỜI BỆNH", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("F" + rowIndex, "G" + rowIndex, "KẾ TOÁN", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "B" + rowIndex, "( ký ghi rõ họ tên )", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("C" + rowIndex, "D" + rowIndex, "( ký ghi rõ họ tên )", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("F" + rowIndex, "G" + rowIndex, "(ký ghi rõ họ tên)", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);

                rowIndex += 1;
                rowIndex += 1;
                rowIndex += 1;

                string tenBacSi = "";
                string tenbenhnhan = "";
                if(data.NguoiDung != null)
                {
                    tenbenhnhan = data.NguoiDung.HoTen;
                }
                if (data.DonThuoc != null)
                {
                    var bacsi = db.NguoiDungs.FirstOrDefault(x => x.Id == data.DonThuoc.BacSiID);
                    
                    if (bacsi != null)
                    {
                        tenBacSi = bacsi.HoTen;
                    }
                }
                ws.AddValue("A" + rowIndex, "B" + rowIndex,tenBacSi, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("C" + rowIndex, "D" + rowIndex, tenbenhnhan, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                
                string fileName1 = "\\Export\\DTBN_HD" + DateTime.Now.ToString("ddMMyyyy") + data.KhamBenh.MaSoKhamBenh + ".pdf";
                string filePath1 = HttpContext.Server.MapPath("~" + fileName1);


                //Save
                wb.SaveAs(filePath);
                wb.SaveAs(filePath, XlFileFormat.xlOpenXMLWorkbook, missing, missing, false, false
                    , XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlUserResolution
                    , true, missing, missing, missing);
                wb.Saved = true;
                wb.Close(true, missing, missing);
                ExportWorkbookToPdf(filePath, filePath1);
                //wb.Close();
                //thoát và thu hồi bộ nhớ cho COM
                ws.ReleaseObject();
                wb.ReleaseObject();

                return Json(new { kq = "ok", data = filePath1, msg = "Thành công!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { kq = "err", msg = "Đã xảy ra lỗi khi lưu dữ liệu. Kiểm tra File" }, JsonRequestBehavior.AllowGet);
            }
            finally
            {
                if (xlApp != null)
                {
                    xlApp.Quit();
                }
                xlApp.ReleaseObject();
            }
        }


        [Route("export/export-prescription")]
        public ActionResult ExportDonThuoc(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            Application xlApp = new Application();
            if (xlApp == null)
            {
                return Json(new { kq = "err", msg = "Lỗi không thể sử dụng được thư viện EXCEL" }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                var config = db.AppConfigs.FirstOrDefault();
                var data = (from a in db.KhamBenhs.ToList()
                            join b in db.NguoiDungs on a.NguoiDungID equals b.Id into b1
                            from b in b1.DefaultIfEmpty()
                            join c in db.ChiTietKhamBenhs on a.Id equals c.KhamBenhID into c1
                            from c in c1.DefaultIfEmpty()
                            join d in db.SuDungDichVus on a.Id equals d.KhamBenhID into d1
                            from d in d1.DefaultIfEmpty()
                            join e in db.DonThuocs on a.Id equals e.KhamBenhID into e1
                            from e in e1.DefaultIfEmpty()
                            where a.Id == id
                            select new ThongTinKhamBenh
                            {
                                NguoiDung = b,
                                KhamBenh = a,
                                ChiTietKhamBenh = c,
                                DonThuoc = e,
                                DichVu = d
                            }
                    )
                    .OrderByDescending(x => x.KhamBenh.NgayKham).FirstOrDefault();

                if (data == null)
                {
                    return Json(new { kq = "err", msg = "Lỗi không có hóa đơn nào" }, JsonRequestBehavior.AllowGet);
                }

                string fileName = "\\Export\\DonThuoc_" + DateTime.Now.ToString("ddMMyyyy") + "_" + data.KhamBenh.MaSoKhamBenh + ".xls";
                string filePath = HttpContext.Server.MapPath("~" + fileName);

                xlApp.DisplayAlerts = false;
                xlApp.Visible = false;
                object missing = Type.Missing;
                Workbook wb = xlApp.Workbooks.Add(missing);
                Worksheet ws = (Worksheet)wb.Worksheets[1];

                int fontSizeTieuDe = 18;
                int fontSizeTenTruong = 14;
                int fontSizeNoiDung = 12;
                //Info
                ws.AddValue("A1", "C1", "TRUNG TÂM Y TẾ ABC", fontSizeTieuDe, true, XlHAlign.xlHAlignCenter, true, 20);
                ws.AddValue("A2", "C2", "BỆNH VIỆN ĐA KHOA ", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true);
                ws.AddValue("A3", "C3", "TRẠM Y TẾ", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false);
                ws.AddValue("F1", "G1", "Mẫu số 03/TYT", fontSizeNoiDung, true, XlHAlign.xlHAlignCenter, false);
                ws.AddValue("F2", "G2", data.KhamBenh.STT, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false);
                ws.AddValue("F3", "G3", "Mã số khám bệnh :" + data.KhamBenh.MaSoKhamBenh, fontSizeNoiDung, true, XlHAlign.xlHAlignCenter, false);
                ws.AddValue("B5", "G5", "ĐƠN THUỐC", fontSizeTieuDe, true, XlHAlign.xlHAlignCenter, false, 20);

                ws.AddValue("A7", "B7", "I. Thông tin bệnh nhân", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("D7", "D7", "Mức hưởng : " + data.NguoiDung.TLMienGiam + "%", fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("A8", "D8", "Họ tên người bệnh :      " + data.NguoiDung.HoTen.ToUpper(), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("E8", "F8", "Ngày sinh :" + data.NguoiDung.NgaySinh.Value.ToString("dd/MM/yyyy"), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 15);
                ws.AddValue("G8", "G8", "Giới tính:" + EnumGioiTinh.ToString(data.NguoiDung.GioiTinh), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("A9", "D9", "Địa chỉ : " + data.NguoiDung.DiaChi, fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);

                ws.AddValue("A10", "C10", " Số BHYT: " + (data.NguoiDung.SoBHYT != null ? data.NguoiDung.SoBHYT : ""), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("D10", "E10", "Áp dụng từ : " + (data.NguoiDung.SoBHYT != null ? data.NguoiDung.NgayApDungBHYT.Value.ToString("dd/MM/yyyy") : ""), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("F10", "G10", " đến : " + (data.NguoiDung.SoBHYT != null ? data.NguoiDung.NgayHetHanBHYT.Value.ToString("dd/MM/yyyy") : ""), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                string chuanDoan = "";
                string ICD = "";
                if (data.ChiTietKhamBenh != null)
                {
                    if (!string.IsNullOrEmpty(data.ChiTietKhamBenh.ChuanDoanBenh))
                    {
                        chuanDoan = data.ChiTietKhamBenh.ChuanDoanBenh;
                    }
                    if (!string.IsNullOrEmpty(data.ChiTietKhamBenh.ChuanDoanICD))
                    {
                        var benh = db.Benhs.FirstOrDefault(x => x.Id == data.ChiTietKhamBenh.BenhID);
                        if(benh != null)
                        {
                            ICD = benh.MaBenhICD + " - " + benh.Ten;
                        }else
                        {
                            ICD = data.ChiTietKhamBenh.ChuanDoanICD;
                        }
                       
                    }
                }
                ws.AddValue("A11", "G11", " Chuẩn đoán  : " + ICD, fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("A12", "G12", " Chuẩn đoán kèm theo  : " + chuanDoan, fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("A13", "C13", "II.Danh sách thuốc", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);
                int rowStart = 15, rowIndex = 16;
                //Header
                ws.AddValue("A" + rowStart, "A" + rowIndex, "STT", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("B" + rowStart, "C" + rowIndex, "Tên thuốc", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("D" + rowStart, "D" + rowIndex, "ĐVT", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 10);
                ws.AddValue("E" + rowStart, "E" + rowIndex, "Số lượng", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 10);
                ws.AddValue("F" + rowStart, "G" + rowIndex, "Cách sử dụng", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.get_Range("A" + rowStart, "G" + rowIndex).SetBorderAround();
                rowIndex += 1;
                var donThuoc = (from a in db.ChiTietDonThuocs.ToList()
                                join b in db.Thuocs on a.ThuocID equals b.Id into b1
                                from b in b1.DefaultIfEmpty()
                                where data.DonThuoc == null ? false : a.DonThuocID == data.DonThuoc.Id
                                select new
                                {
                                    Thuoc = b,
                                    ChiTiet = a
                                }).ToList();
                int i = 0;
                foreach (var item in donThuoc)
                {
                    ws.AddValue("A" + rowIndex, "A" + rowIndex, (++i), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                    ws.AddValue("B" + rowIndex, "C" + rowIndex, item.Thuoc.Ten, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                    ws.AddValue("D" + rowIndex, "D" + rowIndex,item.Thuoc.DonVi , fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 10);
                    ws.AddValue("E" + rowIndex, "E" + rowIndex,item.ChiTiet.SoLuong , fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 10);
                    ws.AddValue("F" + rowIndex, "G" + rowIndex, (string.IsNullOrEmpty(item.ChiTiet.CachSuDung)?"" : item.ChiTiet.CachSuDung) + " - "+ (item.ChiTiet.HamLuongSuDung == null ? "": item.ChiTiet.HamLuongSuDung + "lần/ngày"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);

                    rowIndex += 1;
                }
                ws.AddValue("B" + rowIndex, "C" + rowIndex, "Cộng khoản : "+ i, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.get_Range("A" + rowStart, "G" + rowIndex).SetBorderAround();
                // SIGN
                rowIndex += 1;
                ws.AddValue("E" + rowIndex, "G" + rowIndex, "Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;

                ws.AddValue("F" + rowIndex, "G" + rowIndex, "BÁC SĨ KÊ ĐƠN", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("F" + rowIndex, "G" + rowIndex, "(ký ghi rõ họ tên)", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                rowIndex += 1;
                rowIndex += 1;

                string tenBacSi = "";
                if(data.DonThuoc != null)
                {
                    var bacsi = db.NguoiDungs.FirstOrDefault(x => x.Id == data.DonThuoc.BacSiID);
                    if(bacsi != null)
                    {
                        tenBacSi = bacsi.HoTen;
                    }
                }
                ws.AddValue("F" + rowIndex, "G" + rowIndex, tenBacSi, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                string fileName1 = "\\Export\\DonThuoc_HD_" + DateTime.Now.ToString("ddMMyyyy") + data.KhamBenh.MaSoKhamBenh + ".pdf";
                string filePath1 = HttpContext.Server.MapPath("~" + fileName1);


                //Save
                wb.SaveAs(filePath);
                wb.SaveAs(filePath, XlFileFormat.xlOpenXMLWorkbook, missing, missing, false, false
                    , XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlUserResolution
                    , true, missing, missing, missing);
                wb.Saved = true;
                wb.Close(true, missing, missing);
                ExportWorkbookToPdf(filePath, filePath1);
                //wb.Close();
                //thoát và thu hồi bộ nhớ cho COM
                ws.ReleaseObject();
                wb.ReleaseObject();

                return Json(new { kq = "ok", data = filePath1, msg = "Thành công!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { kq = "err", msg = "Đã xảy ra lỗi khi lưu dữ liệu. Kiểm tra File" }, JsonRequestBehavior.AllowGet);
            }
            finally
            {
                if (xlApp != null)
                {
                    xlApp.Quit();
                }
                xlApp.ReleaseObject();
            }
        }

        [Route("kham-benh/get-phong")]
        public ActionResult GetPhongByKhoa(int? id = null, int? select = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            var phong = db.Phongs.Where(x => x.KhoaID == id && x.TrangThaiID == EnumStatus.ACTIVE && x.LoaiPhong == EnumLoaiPhong.PHONG_KHAM).ToList();
            string cbx = "<option value=\"\">Chọn phòng</option>";
            foreach (var item in phong)
            {
                cbx += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Ten, (select != null && select == item.Id) ? "selected" : "");
            }


            string cbxBacSi = "<option value=\"\">Chọn bác sĩ </option>";
            var bacsi = db.NguoiDungs.Where(x => x.TrangThaiID == EnumStatus.ACTIVE && x.LoaiNguoiDung == EnumUserType.BAC_SI && x.KhoaID == id).OrderByDescending(x => x.Id).ToList();
            foreach (var item in bacsi)
            {
                cbxBacSi += string.Format("<option value=\"{0}\">{1}</option>", item.Id, item.HoTen);
            }
            ViewBag.cbxBacSi = cbxBacSi;
            var obj = new
            {
                cbxPhong = cbx,
                cbxBacSi = cbxBacSi

            };
            return Json(new { kq = "ok", data = obj, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }


        [Route("kham-benh/get-thong-tin-phong")]
        public ActionResult GetBacSiByPhong(int? id = null, int? select = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });

            var phong = db.Phongs.FirstOrDefault(x => x.Id == id && x.TrangThaiID == EnumStatus.ACTIVE && x.LoaiPhong == EnumLoaiPhong.PHONG_KHAM);
            string cbxBacSi = "<option value=\"\">Chọn bác sĩ </option>";
            if (phong != null)
            {
                var bacsi = db.NguoiDungs.Where(x => x.TrangThaiID == EnumStatus.ACTIVE && x.LoaiNguoiDung == EnumUserType.BAC_SI && x.KhoaID == id).OrderByDescending(x => x.Id).ToList();
                foreach (var item in bacsi)
                {
                    cbxBacSi += string.Format("<option value=\"{0}\">{1}</option>", item.Id, item.HoTen);
                }
                ViewBag.cbxBacSi = cbxBacSi;
            }
            var obj = new
            {
                cbxBacSi = cbxBacSi

            };
            return Json(new { kq = "ok", data = obj, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }

        [Route("kham-benh/get-giuong")]
        public ActionResult GetGiuongByPhong(int? id = null, int? select = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            var giuong = db.Giuongs.Where(x => x.PhongID == id && x.TrangThaiID == EnumStatus.ACTIVE).ToList();
            string cbx = "<option value=\"\">Chọn giường</option>";
            foreach (var item in giuong)
            {
                cbx += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Ten, (select != null && select == item.Id) ? "selected" : "");
            }
            return Json(new { kq = "ok", data = cbx, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("dich-vu/get-info")]
        public ActionResult GetDichVuById(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            if(id == null)
                return Json(new { kq = "err",  msg = "Không xác định dịch vụ!" }, JsonRequestBehavior.AllowGet);
            var dichvu = db.DichVus.FirstOrDefault(x => x.Id == id);
            if (dichvu == null)
                return Json(new { kq = "err", msg = "Không xác định dịch vụ!" }, JsonRequestBehavior.AllowGet);
            return Json(new { kq = "ok", data = dichvu, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }
    }
}