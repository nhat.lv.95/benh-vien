﻿using BASICAUTHORIZE.AGRICH;
using BenhVien.MODEL;
using BenhVien.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BenhVien.UI.Controllers
{
    [RouteArea("QuanTri", AreaPrefix = "admin")]
    [Route("{action}")]
    public class CanLamSangController : Controller
    {
        public BenhVienEntities db = new BenhVienEntities();
        [Route("can-lam-sang/main-page")]
        public ActionResult MainPage()
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });
            return View();
        }
        [Route("can-lam-sang/list")]
        public ActionResult List(string keyword = "", int? status = EnumTrangThaiDichVu.CHUA_THUC_HIEN, string fromDate = "", string toDate = "", int sotrang = 1, int tongsodong = 5)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });
            DateTime? dateFrom = fromDate.ToDateTime();
            DateTime? dateTo = toDate.ToDateTime();
            if (keyword != "")
                keyword = keyword.BoDauTiengViet().ToLower();
            var data = (from a in db.SuDungDichVus.ToList()
                        join b in db.ChiTietSuDungDichVus on a.Id equals b.SuDungDichVuID 
                        join c in db.DichVus on b.DichVuID equals c.Id into c1
                        from c in c1.DefaultIfEmpty()
                        join d in db.NguoiDungs on a.BacSiID equals d.Id into d1
                        from d in d1.DefaultIfEmpty()
                        join e in db.KhamBenhs on a.KhamBenhID equals e.Id into e1
                        from e in e1.DefaultIfEmpty()
                        join f in db.NhapViens on a.NhapVienID equals f.Id into f1
                        from f in f1.DefaultIfEmpty()
                        where (keyword == "" || (keyword != "" && (c.Ten.BoDauTiengViet().ToLower().Contains(keyword))
                        || (c.MaDichVu.BoDauTiengViet().ToLower().Contains(keyword))
                         || (a.MaPhieu.BoDauTiengViet().ToLower().Contains(keyword))
                        )
                        )
                        && (status == null ? true : a.TrangThaiID == status)
                        select new
                        {
                            SuDungDichVu = a,
                            NguoiDung = d,
                            ChiTietSuDungDichVu = b,
                            DichVu = c,
                            KhamBenh = e,
                            NhapVien = f,
                        }).Where(a => (string.IsNullOrEmpty(fromDate) ? true : a.SuDungDichVu.NgayTao.Value.ToString("dd/MM/yyyy").ToDateTime() >= dateFrom)
                            && (string.IsNullOrEmpty(toDate) ? true : a.SuDungDichVu.NgayTao.Value.ToString("dd/MM/yyyy").ToDateTime() <= dateTo))
                        .OrderByDescending(x => x.SuDungDichVu.NgayTao).ToList();

            var list = data.GroupBy(x => x.SuDungDichVu.Id).Select(x => new SuDungDichVuThongTin
            {

                SuDungDichVu = x.FirstOrDefault(y => y.SuDungDichVu.Id == x.Key).SuDungDichVu,
                NguoiDung = x.FirstOrDefault(y => y.SuDungDichVu.Id == x.Key).NguoiDung,
                NhapVien = x.FirstOrDefault(y => y.SuDungDichVu.Id == x.Key).NhapVien,
                KhamBenh = x.FirstOrDefault(y => y.SuDungDichVu.Id == x.Key).KhamBenh,
                ChiTiet = x.Where(y => y.SuDungDichVu.Id == x.Key).Select(t => new ChiTietSuDungDichVuThongTin
                {
                    DichVu = t.DichVu,
                    ChiTietSuDungDichVu = t.ChiTietSuDungDichVu
                }).ToList()
            }).ToList();

            int tongso = list.Count();

            sotrang = sotrang <= 0 ? 1 : sotrang;
            tongsodong = tongsodong <= 0 ? 10 : tongsodong;
            int tongsotrang = tongso % tongsodong > 0 ? tongso / tongsodong + 1 : tongso / tongsodong;
            tongsotrang = tongsotrang <= 0 ? 1 : tongsotrang;
            sotrang = sotrang > tongsotrang ? tongsotrang - 1 : sotrang - 1;
            ViewBag.sotrang = sotrang + 1;
            ViewBag.tongsotrang = tongsotrang;
            ViewBag.tongso = tongso;
            return PartialView(list == null ? list : list.Skip(sotrang * tongsodong).Take(tongsodong));
        }


        [Route("can-lam-sang/change-status")]
        public ActionResult Change_Status(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var canlamsang = db.SuDungDichVus.FirstOrDefault(x => x.Id == id);
            if (canlamsang == null)
                return Json(new { kq = "err", msg = "Không xác định!" }, JsonRequestBehavior.AllowGet);
            return PartialView(canlamsang);
        }

        [HttpPost]
        [Route("can-lam-sang/change-status")]
        public ActionResult Change_Status(int? id = null, int? status = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            if (status == null)
                return Json(new { kq = "err", msg = "Trạng thái không xác định!" }, JsonRequestBehavior.AllowGet);
            var canlamsang = db.SuDungDichVus.FirstOrDefault(x => x.Id == id);
            if (canlamsang == null)
                return Json(new { kq = "err", msg = "Không xác định!" }, JsonRequestBehavior.AllowGet);
            canlamsang.TrangThaiID = status;
            db.SaveChanges();
            return Json(new { kq = "ok", data = canlamsang.TrangThaiID, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }
    }
}