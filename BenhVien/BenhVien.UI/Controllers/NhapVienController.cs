﻿using BASICAUTHORIZE.AGRICH;
using BenhVien.MODEL;
using BenhVien.UI.Models;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BenhVien.UI.Controllers
{
    [RouteArea("QuanTri", AreaPrefix = "admin")]
    [Route("{action}")]
    public class NhapVienController : Controller
    {
        public BenhVienEntities db = new BenhVienEntities();

        [Route("noi-tru/dang-ky")]
        public ActionResult MainPage()
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });

            string cbxBenhNhan = "<option value=\"\">Chọn bệnh nhân...</option>";
            var benhNhan = db.NguoiDungs.Where(x => x.TrangThaiID == EnumStatus.ACTIVE && x.LoaiNguoiDung == EnumUserType.BENH_NHAN).ToList();
            foreach (var item in benhNhan)
            {
                cbxBenhNhan += string.Format("<option value=\"{0}\">{1}</option>", item.Id, item.HoTen + " - " + item.SDT);
            }
            ViewBag.cbxBenhNhan = cbxBenhNhan;

            return View();
        }
        [Route("noi-tru/dang-ky-list")]
        public ActionResult List(int? status = EnumTrangThaiNhapVien.DANG_KY, int? userID = null, string fromDate = "", string toDate = "", int sotrang = 1, int tongsodong = 5)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });

            DateTime? dateFrom = fromDate.ToDateTime();
            DateTime? dateTo = toDate.ToDateTime();
            var list = (from a in db.NhapViens.ToList()
                        join b in db.NguoiDungs on a.NguoiDungID equals b.Id into b1
                        from b in b1.DefaultIfEmpty()
                        join c in db.ChiTietNhapViens.ToList() on a.Id equals c.NhapVienID into c1
                        from c in c1.DefaultIfEmpty()
                        where b!= null && (userID == null ? true : a.NguoiDungID == userID)
                        && a.TrangThaiID == status
                        select new ThongTinNhapVien
                        {
                            NguoiDung = b,
                            NhapVien = a,
                            ChiTietNhapVien = c
                        }
                        )
                        //.Where(a => (string.IsNullOrEmpty(fromDate) ? true : a.NhapVien.NgayNhapVien.Value.ToString("dd/MM/yyyy").ToDateTime() >= dateFrom)
                        //    && (string.IsNullOrEmpty(toDate) ? true : a.NhapVien.NgayNhapVien.Value.ToString("dd/MM/yyyy").ToDateTime() <= dateTo))
                        .OrderByDescending(x => x.NhapVien.NgayNhapVien).ToList();

            int tongso = list.Count();

            sotrang = sotrang <= 0 ? 1 : sotrang;
            tongsodong = tongsodong <= 0 ? 10 : tongsodong;
            int tongsotrang = tongso % tongsodong > 0 ? tongso / tongsodong + 1 : tongso / tongsodong;
            tongsotrang = tongsotrang <= 0 ? 1 : tongsotrang;
            sotrang = sotrang > tongsotrang ? tongsotrang - 1 : sotrang - 1;
            ViewBag.sotrang = sotrang + 1;
            ViewBag.tongsotrang = tongsotrang;
            ViewBag.tongso = tongso;
            return PartialView(list == null ? list : list.Skip(sotrang * tongsodong).Take(tongsodong));
        }

        [Route("noi-tru/benh-nhan-update")]
        public ActionResult BenhNhan_Update(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });

            var dangky = (from a in db.NhapViens.ToList()
                          join b in db.NguoiDungs on a.NguoiDungID equals b.Id into b1
                          from b in b1.DefaultIfEmpty()
                          where a.Id == id
                          select new ThongTinNhapVien
                          {
                              NguoiDung = b,
                              NhapVien = a
                          }
                     )
                     .OrderByDescending(x => x.NhapVien.NgayNhapVien).FirstOrDefault();
            string cbxProvince = "<option value=\"\">Chọn tỉnh/ thành phố</option>";
            var province = db.Provinces.OrderByDescending(x => x.STT).ToList();
            foreach (var item in province)
            {
                cbxProvince += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Name, (dangky != null && dangky.NguoiDung.ThanhPhoID == item.Id) ? "selected" : "");
            }
            ViewBag.cbxProvince = cbxProvince;
            string cbxKhoa = "<option value=\"\">Chọn khoa</option>";
            var khoa = db.Khoas.Where(x => x.TrangThaiID == EnumStatus.ACTIVE).OrderByDescending(x => x.Id).ToList();
            foreach (var item in khoa)
            {
                cbxKhoa += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Ten, (dangky != null && dangky.NguoiDung.KhoaID == item.Id) ? "selected" : "");
            }
            ViewBag.cbxKhoa = cbxKhoa;


            string cbxBenhNhan = "<option value=\"\">Chọn bệnh nhân</option>";
            var benhNhan = db.NguoiDungs.Where(x => x.TrangThaiID == EnumStatus.ACTIVE && x.LoaiNguoiDung == EnumUserType.BENH_NHAN).OrderByDescending(x => x.Id).ToList();
            foreach (var item in benhNhan)
            {
                cbxBenhNhan += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.HoTen, (dangky != null && dangky.NguoiDung.Id == item.Id) ? "selected" : "");
            }
            ViewBag.cbxBenhNhan = cbxBenhNhan;
            return PartialView(dangky);
        }
        [Route("noi-tru/benh-nhan-update")]
        [HttpPost, ValidateInput(false)]
        public ActionResult BenhNhan_Update(NhapVien nhapVien, NguoiDung user, HttpPostedFileBase _Avatar = null, string NgaySinh = "", string NgayApDungBHYT = "", string NgayHetHanBHYT = "")
        {
            try
            {
                UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
                if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                    return RedirectToAction("Index", "Home", new { area = "" });
                if (!user.Email.checkEmail())
                    return Json(new { kq = "err", msg = "Email không đúng !" }, JsonRequestBehavior.AllowGet);
                if (!user.SDT.checkSoDienThoai())
                    return Json(new { kq = "err", msg = "Số điện thoại không hợp lệ !" }, JsonRequestBehavior.AllowGet);

                if (nhapVien.ApDungBHYT == EnumStatus.ACTIVE)
                {
                    if (string.IsNullOrEmpty(user.SoBHYT))
                        return Json(new { kq = "err", msg = "Không thể áp dụng BYHT vì chưa nhập số BHYT" }, JsonRequestBehavior.AllowGet);
                    if (string.IsNullOrEmpty(NgayApDungBHYT))
                        return Json(new { kq = "err", msg = "Không thể áp dụng BYHT. Ngày áp dụng thẻ BHYT không chính xác" }, JsonRequestBehavior.AllowGet);
                    if (string.IsNullOrEmpty(NgayHetHanBHYT))
                        return Json(new { kq = "err", msg = "Không thể áp dụng BYHT. Ngày hết hạn thẻ BHYT không chính xác" }, JsonRequestBehavior.AllowGet);
                }
                DateTime? dateOfBirth = NgaySinh.ToDateTime();
                DateTime? ngayApDung = NgayApDungBHYT.ToDateTime();
                DateTime? ngayHetHan = NgayHetHanBHYT.ToDateTime();
                var appConfigs = db.AppConfigs.FirstOrDefault();
                int userID = 0;
                if (nhapVien.NguoiDungID == null)
                {
                    // chua co user thy them vao
                    if (user.Id == 0)
                    {
                        if (_Avatar != null)
                        {
                            string rootPathImage = string.Format("~/Files/nguoi-dung/avatar/{0}", DateTime.Now.ToString("ddMMyyyy"));
                            string filePathImage = System.IO.Path.Combine(Request.MapPath(rootPathImage));
                            string[] fileImage = _Avatar.uploadFile(rootPathImage, filePathImage);
                            user.Avatar = fileImage[1];
                        }
                        user.Code = Functions.CreateCode();
                        user.NgayApDungBHYT = ngayApDung;
                        user.NgayHetHanBHYT = ngayHetHan;
                        user.NgaySinh = dateOfBirth;
                        user.LoaiNguoiDung = EnumUserType.BENH_NHAN;
                        user.TLMienGiam = nhapVien.TLMienGiam;
                        using (var context = new BenhVienEntities())
                        {
                            context.NguoiDungs.Add(user);
                            context.SaveChanges();
                            userID = user.Id;
                        }
                    }
                    else
                    {
                        userID = user.Id;
                        using (var context = new BenhVienEntities())
                        {
                            context.SaveChanges();
                        }
                    }

                }
                else
                {
                    userID = nhapVien.NguoiDungID.Value;

                }
                if (nhapVien.Id == 0)
                {
                    nhapVien.MaBenhAn = Functions.CreateCode();
                    nhapVien.NgayNhapVien = DateTime.Now;
                    nhapVien.NguoiDungID = userID;
                    db.NhapViens.Add(nhapVien);
                    db.SaveChanges();
                }
                else
                {
                    var old = db.NhapViens.FirstOrDefault(x => x.Id == nhapVien.Id);
                    if (old == null)
                        return Json(new { kq = "err", msg = "Thông tin không xác định!" }, JsonRequestBehavior.AllowGet);
                    //old.TrangThaiID = nhapVien.TrangThaiID;
                    old.TLMienGiam = nhapVien.TLMienGiam;
                    old.ApDungBHYT = nhapVien.ApDungBHYT;
                    old.KieuNhapVien = nhapVien.KieuNhapVien;
                    var userOld = db.NguoiDungs.FirstOrDefault(x => x.Id == userID);
                    userOld.TLMienGiam = user.TLMienGiam;
                    db.SaveChanges();
                }
                return Json(new { kq = "ok", msg = "Thành công!" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { kq = "err", msg = "Lỗi phát sinh yêu cầu" }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("noi-tru/thong-tin")]
        public ActionResult Update(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            var dangky = (from a in db.NhapViens.ToList()
                          join b in db.NguoiDungs on a.NguoiDungID equals b.Id into b1
                          from b in b1.DefaultIfEmpty()
                          join c in db.ChiTietNhapViens on a.Id equals c.NhapVienID into c1
                          from c in c1.DefaultIfEmpty()
                          join d in db.SuDungDichVus on a.Id equals d.NhapVienID into d1
                          from d in d1.DefaultIfEmpty()
                          join e in db.DonThuocs on a.Id equals e.NhapVienID into e1
                          from e in e1.DefaultIfEmpty()
                          where a.Id == id
                          select new ThongTinChiTietNhapVien
                          {
                              NguoiDung = b,
                              NhapVien = a,
                              ChiTietNhapVien = c,
                              DonThuoc = e,
                              DichVu = d
                          }
                     )
                     .OrderByDescending(x => x.NhapVien.NgayNhapVien).FirstOrDefault();

            string cbxBenh = "<option value=\"\">Chọn bệnh </option>";
            var benh = db.Benhs.Where(x => x.TrangThaiID == EnumStatus.ACTIVE).OrderByDescending(x => x.Id).ToList();
            foreach (var item in benh)
            {
                cbxBenh += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Ten, (dangky != null && dangky.ChiTietNhapVien != null && dangky.ChiTietNhapVien.BenhID == item.Id) ? "selected" : "");
            }
            ViewBag.cbxBenh = cbxBenh;
            string cbxKhoa = "<option value=\"\">Chọn khoa </option>";
            var khoa = db.Khoas.Where(x => x.TrangThaiID == EnumStatus.ACTIVE).OrderByDescending(x => x.Id).ToList();
            foreach (var item in khoa)
            {
                cbxKhoa += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Ten, (dangky != null && dangky.ChiTietNhapVien != null && dangky.ChiTietNhapVien.KhoaID == item.Id) ? "selected" : "");
            }
            ViewBag.cbxKhoa = cbxKhoa;



            return PartialView(dangky);
        }
        [Route("noi-tru/thong-tin")]
        [HttpPost, ValidateInput(false)]
        public ActionResult Update(ChiTietNhapVien chitiet, int? TrangThaiID = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            var nhapvien = db.NhapViens.FirstOrDefault(x => x.Id == chitiet.NhapVienID);
            if (nhapvien == null)
                return Json(new { kq = "err", msg = "Không tìm thấy bệnh án!" }, JsonRequestBehavior.AllowGet);

            if (TrangThaiID == EnumTrangThaiNhapVien.XUAT_VIEN)
            {
                // check xem xet nghiem ok chua
                var dichvu = db.SuDungDichVus.Where(x => x.NhapVienID == nhapvien.Id).ToList();
                foreach (var item in dichvu)
                {
                    var chitietdichvu = db.ChiTietSuDungDichVus.Where(x => x.SuDungDichVuID == item.Id);
                    foreach (var d in chitietdichvu)
                    {
                        if (d.TrangThaiID != EnumTrangThaiDichVu.HOAN_THANH)
                            return Json(new { kq = "err", msg = "Các xét nghiệm đang thực hiện. Không thể xuất viện" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            nhapvien.TrangThaiID = TrangThaiID;
            if (chitiet.Id == 0)
            {
                chitiet.NgayVaoKhoa = DateTime.Now;
                db.ChiTietNhapViens.Add(chitiet);
                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // up date
                var old = db.ChiTietNhapViens.FirstOrDefault(x => x.Id == chitiet.Id);
                if (old == null)
                    return Json(new { kq = "err", msg = "Thông tin không xác định!" }, JsonRequestBehavior.AllowGet);
                old.BacSiID = chitiet.BacSiID;
                old.YTaID = chitiet.YTaID;
                old.ChuanDoanTamThoi = chitiet.ChuanDoanTamThoi;
                old.ChuanDoanTuKhoaTruoc = chitiet.ChuanDoanTuKhoaTruoc;
                old.TrangThaiID = chitiet.TrangThaiID;
                old.BenhID = chitiet.BenhID;
                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
        }
        [Route("noi-tru/get-phong")]
        public ActionResult GetPhongByKhoa(int? id = null, int? select = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            var phong = db.Phongs.Where(x => x.KhoaID == id && x.TrangThaiID == EnumStatus.ACTIVE && x.LoaiPhong == EnumLoaiPhong.PHONG_BENH).ToList();
            string cbx = "<option value=\"\">Chọn phòng</option>";
            foreach (var item in phong)
            {
                cbx += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Ten, (select != null && select == item.Id) ? "selected" : "");
            }
            //var dangky = db.ChiTietNhapViens.FirstOrDefault(x => x.Id == chitietID);
            string cbxBacSi = "<option value=\"\">Chọn bác sĩ </option>";
            var bacsi = db.NguoiDungs.Where(x => x.TrangThaiID == EnumStatus.ACTIVE && x.LoaiNguoiDung == EnumUserType.BAC_SI && x.KhoaID == id).OrderByDescending(x => x.Id).ToList();
            foreach (var item in bacsi)
            {
                cbxBacSi += string.Format("<option value=\"{0}\">{1}</option>", item.Id, item.HoTen);
            }
            ViewBag.cbxBacSi = cbxBacSi;

            string cbxYta = "<option value=\"\">Chọn điều dưỡng </option>";
            var yta = db.NguoiDungs.Where(x => x.TrangThaiID == EnumStatus.ACTIVE && x.LoaiNguoiDung == EnumUserType.Y_TA && x.KhoaID == id).OrderByDescending(x => x.Id).ToList();
            foreach (var item in yta)
            {
                cbxYta += string.Format("<option value=\"{0}\">{1}</option>", item.Id, item.HoTen);
            }
            ViewBag.cbxYta = cbxYta;

            var obj = new
            {
                cbxPhong = cbx,
                cbxBacSi = cbxBacSi,
                cbxYTa = cbxYta

            };



            return Json(new { kq = "ok", data = obj, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }

        [Route("noi-tru/get-giuong")]
        public ActionResult GetGiuongByPhong(int? khoa = null, int? phong = null, int? chitietID = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            var giuong = (from a in db.Giuongs.ToList()
                          join b in db.Phongs on a.PhongID equals b.Id into b1
                          from b in b1.DefaultIfEmpty()
                          join c in db.Khoas on b.KhoaID equals c.Id into c1
                          from c in c1.DefaultIfEmpty()
                          where b.KhoaID == khoa && a.PhongID == phong
                          && a.TrangThaiID == EnumTrangThaiGiuongBenh.KHONG_SU_DUNG
                          && b.TrangThaiID == EnumStatus.ACTIVE
                          && c.TrangThaiID == EnumStatus.ACTIVE
                          select a).ToList();
            var chitiet = db.ChiTietNhapViens.FirstOrDefault(x => x.Id == chitietID);
            string cbx = "<option value=\"\">Chọn giường bệnh</option>";
            foreach (var item in giuong)
            {
                cbx += string.Format("<option value=\"{0}\">{1}</option>", item.Id, item.Ten);
            }
            ViewBag.cbxGiuong = cbx;
            List<ChiTietGiuongBenh> chiTietGiuongBenh = new List<ChiTietGiuongBenh>();
            if (chitiet != null)
            {
                chiTietGiuongBenh = db.ChiTietGiuongBenhs.Where(x => x.ChiTietNhapVienID == chitiet.Id).ToList();
            }
            ViewBag.ChiTiet = chiTietGiuongBenh;
            ViewBag.Giuong = giuong;
            return PartialView(chitiet);
        }

        [HttpPost]
        [Route("noi-tru/get-giuong")]
        public ActionResult GetGiuongByPhong(ChiTietNhapVien chiTiet, List<ChiTietGiuongBenh> list = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            int chiTietID = 0;
            if (chiTiet.Id == 0)
            {
                chiTiet.NgayVaoKhoa = DateTime.Now;
                using (var context = new BenhVienEntities())
                {
                    context.ChiTietNhapViens.Add(chiTiet);
                    context.SaveChanges();
                    chiTietID = chiTiet.Id;
                }
                foreach (var item in list)
                {
                    item.TrangThaiID = EnumStatus.ACTIVE;
                    item.ChiTietNhapVienID = chiTietID;
                }
                db.ChiTietGiuongBenhs.AddRange(list);
            }
            else
            {
                chiTietID = chiTiet.Id;
                var listChiTiet = db.ChiTietGiuongBenhs.Where(x => x.ChiTietNhapVienID == chiTiet.Id);
                db.ChiTietGiuongBenhs.RemoveRange(listChiTiet);
                foreach (var item in list)
                {
                    item.TrangThaiID = EnumStatus.ACTIVE;
                    item.ChiTietNhapVienID = chiTietID;
                }
                db.ChiTietGiuongBenhs.AddRange(list);
            }
            db.SaveChanges();

            return Json(new { kq = "ok", data = "", msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }

        [Route("noi-tru/dat-coc")]
        public ActionResult DatCocVienPhi(int? nhapVienID = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            var nhapVien = db.NhapViens.FirstOrDefault(x => x.Id == nhapVienID);
            if (nhapVien == null)
                return Json(new { kq = "err", data = "", msg = "Không tìm thấy bệnh án!" }, JsonRequestBehavior.AllowGet);
            var chitiet = db.ChiTietChiPhiNhapViens.Where(x => x.NhapVienID == nhapVien.Id).ToList();
            ViewBag.NhapVien = nhapVien;
            return PartialView(chitiet);
        }

        [HttpPost]
        [Route("noi-tru/dat-coc")]
        public ActionResult DatCocVienPhi(ChiTietNhapVien chiTiet, List<ChiTietChiPhiNhapVien> list = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            int chiTietID = 0;
            if (chiTiet.Id == 0)
            {
                chiTiet.NgayVaoKhoa = DateTime.Now;
                using (var context = new BenhVienEntities())
                {
                    context.ChiTietNhapViens.Add(chiTiet);
                    context.SaveChanges();
                    chiTietID = chiTiet.Id;
                }
                foreach (var item in list)
                {
                    item.StatusID = EnumStatus.ACTIVE;
                    item.NhapVienID = chiTiet.NhapVienID;
                }
                db.ChiTietChiPhiNhapViens.AddRange(list);
            }
            else
            {
                chiTietID = chiTiet.Id;
                var listChiTiet = db.ChiTietChiPhiNhapViens.Where(x => x.NhapVienID == chiTiet.NhapVienID);
                db.ChiTietChiPhiNhapViens.RemoveRange(listChiTiet);
                foreach (var item in list)
                {
                    item.StatusID = EnumStatus.ACTIVE;
                    item.NhapVienID = chiTiet.NhapVienID;
                }
                db.ChiTietChiPhiNhapViens.AddRange(list);
            }
            db.SaveChanges();

            return Json(new { kq = "ok", data = "", msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }



        [Route("noi-tru/theo-doi-hang-ngay")]
        public ActionResult SoTheoDoiHangNgay(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            string cbxDate = "";
            bool check = true;
            var nhapVien = db.NhapViens.FirstOrDefault(x => x.Id == id);
            var soTheoDoi = db.TheoDoiBenhNhans.Where(x => x.NhapVienID == id).OrderByDescending(x => x.Id).ToList();
            foreach (var item in soTheoDoi)
            {
                if (item.NgayTao.Value.ToString("dd/MM/yyyy").Equals(DateTime.Now.ToString("dd/MM/yyyy")))
                {
                    check = false;
                }
                cbxDate += string.Format("<option value=\"{0}\">{1}</option>", item.Id, item.NgayTao.Value.ToString("dd/MM/yyyy HH:ss:mm"));

            }
            if (check)
            {
                cbxDate += string.Format("<option value=\"{0}\" selected>{1}</option>", 0, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            }
            ViewBag.cbxDate = cbxDate;
            return PartialView(nhapVien);
        }

        [Route("noi-tru/chi-tiet-theo-doi-hang-ngay")]
        public ActionResult ChiTietTheoDoiHangNgay(int? theodoiID = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            var theoDoi = db.TheoDoiBenhNhans.FirstOrDefault(x => x.Id == theodoiID);
            return PartialView(theoDoi);

        }
        [HttpPost]
        [Route("noi-tru/theo-doi-hang-ngay")]
        public ActionResult SoTheoDoiHangNgay(TheoDoiBenhNhan theoDoi)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });

            if (theoDoi.Id == 0)
            {
                theoDoi.NgayTao = DateTime.Now;
                theoDoi.NguoiTao = nd_dv.User.Id;
                theoDoi.TrangThai = EnumStatus.ACTIVE;
                db.TheoDoiBenhNhans.Add(theoDoi);
                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // up date
                var old = db.TheoDoiBenhNhans.FirstOrDefault(x => x.Id == theoDoi.Id);
                if (old == null)
                    return Json(new { kq = "err", msg = "Thông tin không xác định!" }, JsonRequestBehavior.AllowGet);
                old.DienBienBenh = theoDoi.DienBienBenh;
                old.YLenhBacSi = theoDoi.DienBienBenh;
                old.Mach = theoDoi.Mach;
                old.NhipTho = theoDoi.NhipTho;
                old.ChieuCao = theoDoi.ChieuCao;
                old.NhietDo = theoDoi.NhietDo;
                old.HuyetAp = theoDoi.HuyetAp;
                old.CanNang = theoDoi.CanNang;
                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("noi-tru/export-excel")]
        public ActionResult ExportExcel(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.BAC_SI))
                return RedirectToAction("Index", "Home", new { area = "" });
            Application xlApp = new Application();
            if (xlApp == null)
            {
                return Json(new { kq = "err", msg = "Lỗi không thể sử dụng được thư viện EXCEL" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                var config = db.AppConfigs.FirstOrDefault();
                var data = (from a in db.NhapViens.ToList()
                            join b in db.NguoiDungs on a.NguoiDungID equals b.Id into b1
                            from b in b1.DefaultIfEmpty()
                            join c in db.ChiTietNhapViens on a.Id equals c.NhapVienID into c1
                            from c in c1.DefaultIfEmpty()
                            join d in db.SuDungDichVus on a.Id equals d.NhapVienID into d1
                            from d in d1.DefaultIfEmpty()
                            join e in db.DonThuocs on a.Id equals e.NhapVienID into e1
                            from e in e1.DefaultIfEmpty()
                            where a.Id == id
                            select new
                            {
                                NguoiDung = b,
                                NhapVien = a,
                                ChiTietNhapVien = c,
                                DonThuoc = e,
                                DichVu = d
                            }
                    )
                    .OrderByDescending(x => x.NhapVien.NgayNhapVien).FirstOrDefault();

                if (data == null)
                {
                    return Json(new { kq = "err", msg = "Lỗi không có hóa đơn nào" }, JsonRequestBehavior.AllowGet);
                }
                if(data.NhapVien.TrangThaiID == EnumTrangThaiNhapVien.DANG_KY || data.NhapVien.TrangThaiID == EnumTrangThaiNhapVien.DANG_NOI_TRU)
                {
                    return Json(new { kq = "err", msg = "Bệnh nhân vẫn đang điều trị. Không thể xuất hóa đơn !" }, JsonRequestBehavior.AllowGet);
                }
                if (data.NhapVien.NgayXuatVien == null)
                {
                    data.NhapVien.NgayXuatVien = DateTime.Now;
                }
                //return Json(new { kq = "err", msg = "Bệnh nhân chưa xuất viện nên không thể xem hóa đơn !" }, JsonRequestBehavior.AllowGet);
                string fileName = "\\Export\\HDXV_" + DateTime.Now.ToString("ddMMyyyy") + "_" + data.NhapVien.MaBenhAn + ".xls";
                string filePath = HttpContext.Server.MapPath("~" + fileName);

                xlApp.DisplayAlerts = false;
                xlApp.Visible = false;
                object missing = Type.Missing;
                Workbook wb = xlApp.Workbooks.Add(missing);
                Worksheet ws = (Worksheet)wb.Worksheets[1];

                int fontSizeTieuDe = 18;
                int fontSizeTenTruong = 14;
                int fontSizeNoiDung = 12;
                //Info
                ws.AddValue("A1", "C1", "TRUNG TÂM Y TẾ ABC", fontSizeTieuDe, true, XlHAlign.xlHAlignCenter, true, 20);
                ws.AddValue("A2", "C2", "BỆNH VIỆN ĐA KHOA ", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true);
                ws.AddValue("A3", "C3", "TRẠM Y TẾ", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false);


                ws.AddValue("F1", "G1", "Mẫu số 03/TYT", fontSizeNoiDung, true, XlHAlign.xlHAlignCenter, false);

                ws.AddValue("F3", "G3", "Mã số khám bệnh :" + data.NhapVien.MaBenhAn, fontSizeNoiDung, true, XlHAlign.xlHAlignCenter, false);




                ws.AddValue("B5", "G5", "BẢNG KÊ KHAI CHI PHÍ KHÁM CHỮA BỆNH", fontSizeTieuDe, true, XlHAlign.xlHAlignCenter, false, 20);
                ws.AddValue("D6", "E6", "TẠI TRẠM Y TẾ", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);


                ws.AddValue("A7", "B7", "I. Thông tin bệnh nhân", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("D7", "D7", "Mức hưởng : " + data.NguoiDung.TLMienGiam + "%", fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);


                ws.AddValue("A8", "D8", "(1) Họ tên người bệnh :      " + data.NguoiDung.HoTen.ToUpper(), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("E8", "F8", "Ngày sinh :" + data.NguoiDung.NgaySinh.Value.ToString("dd/MM/yyyy"), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 15);
                ws.AddValue("G8", "G8", "Giới tính:" + EnumGioiTinh.ToString(data.NguoiDung.GioiTinh), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);



                ws.AddValue("A9", "D9", "(2) Địa chỉ : " + data.NguoiDung.DiaChi, fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);

                ws.AddValue("A10", "C10", "(3) Số BHYT: " + (data.NguoiDung.SoBHYT != null ? data.NguoiDung.SoBHYT : ""), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("D10", "E10", "Áp dụng từ : " + (data.NguoiDung.SoBHYT != null ? data.NguoiDung.NgayApDungBHYT.Value.ToString("dd/MM/yyyy") : ""), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("F10", "G10", " đến : " + (data.NguoiDung.SoBHYT != null ? data.NguoiDung.NgayHetHanBHYT.Value.ToString("dd/MM/yyyy") : ""), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);

                ws.AddValue("A11", "G11", "(4) Vào viện  : " + data.NhapVien.NgayNhapVien.Value.ToString("dd/MM/yyyy HH:mm") + " ra viện :" + data.NhapVien.NgayXuatVien.Value.ToString("dd/MM/yyyy HH:mm"), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);

                ws.AddValue("A12", "E12", "(5) Kiểu nhập viện : " + EnumKieuKhamBenh.ToString(data.NhapVien.KieuNhapVien), fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                string chuanDoan = "";
                string ICD = "";
                if (data.ChiTietNhapVien != null)
                {
                    var benh = db.Benhs.FirstOrDefault(x => x.Id == data.ChiTietNhapVien.BenhID);
                    if (benh != null)
                    {
                        chuanDoan = benh.Ten;
                        ICD = benh.MaBenhICD;
                    }
                }
                ws.AddValue("A13", "E13", "(6) Chuẩn đoán  : " + chuanDoan, fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);
                ws.AddValue("F13", "F13", "(6) Mã bệnh  : " + ICD, fontSizeNoiDung, true, XlHAlign.xlHAlignLeft, false, 20);


                ws.AddValue("A14", "C14", "II.Chi phí khám chữa bệnh", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, false, 20);


                int rowStart = 15, rowIndex = 16;
                //Header
                ws.AddValue("A" + rowStart, "A" + rowIndex, "Nội dung", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("B" + rowStart, "B" + rowIndex, "ĐVT", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("C" + rowStart, "C" + rowIndex, "Số lượng", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 10);
                ws.AddValue("D" + rowStart, "D" + rowIndex, "Đơn giá", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 10);
                ws.AddValue("E" + rowStart, "E" + rowIndex, "Thành tiền", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("F" + rowStart, "G" + rowStart, "Nguồn thanh toán", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("F" + rowIndex, "F" + rowIndex, "BHYT", fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, false, 12);
                ws.AddValue("G" + rowIndex, "G" + rowIndex, "Người bệnh", fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, false, 12);




                ws.get_Range("A" + rowStart, "G" + rowIndex).SetBorderAround();


                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "G" + rowIndex, "1.Ngày giường chuyên khoa", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, true, 12);
                var giuong = (from a in db.ChiTietGiuongBenhs.ToList()
                              join b in db.Giuongs on a.GiuongID equals b.Id into b1
                              from b in b1.DefaultIfEmpty()
                              join c in db.Phongs on b.PhongID equals c.Id into c1
                              from c in c1.DefaultIfEmpty()
                              where data.ChiTietNhapVien == null ? false: a.ChiTietNhapVienID == data.ChiTietNhapVien.Id
                              select new
                              {
                                  ChiTiet = a,
                                  Giuong = b,
                                  Phong = c
                              }).ToList();

                rowIndex += 1;
                 TimeSpan interval = data.NhapVien.NgayXuatVien.Value.Subtract(data.NhapVien.NgayNhapVien.Value);
                var totalDay = interval.Days == 0 ? 1 : interval.Days;
                decimal? tongtiengiuong = 0;
                double? tongtienGiuongBHYT = 0;
                foreach (var item in giuong)
                {
                    double? tonggiuongBHYT = 0;
                    double? tonggiuong = Convert.ToDouble(item.Phong.Gia * totalDay);
                    tongtiengiuong += item.Phong.Gia * totalDay;
                    if (item.Phong.GiamGiaBHYT == EnumStatus.ACTIVE)
                    {
                        tongtienGiuongBHYT += Convert.ToDouble(item.Phong.Gia * totalDay) * data.NguoiDung.TLMienGiam / 100;
                        tonggiuongBHYT = Convert.ToDouble(item.Phong.Gia * totalDay) * data.NguoiDung.TLMienGiam / 100;
                    }
                    dynamic[] val = { item.Giuong.Ten, "ngày",item.Phong.Gia.Value.ToString("#,###") ,totalDay, tonggiuong.Value.ToString("#,###"), tonggiuongBHYT.Value.ToString("#,###"), (Convert.ToDouble(tonggiuong) - tonggiuongBHYT).Value.ToString("#,###") };
                    ws.AddValue("A" + rowIndex, "G" + rowIndex, val, fontSizeNoiDung, false, XlHAlign.xlHAlignLeft, false);
                    rowIndex += 1;
                }
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "D" + rowIndex, "Tổng cộng (1)", fontSizeTenTruong, true, XlHAlign.xlHAlignRight, true, 12);

                ws.AddValue("E" + rowIndex, "E" + rowIndex, tongtiengiuong.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("F" + rowIndex, "F" + rowIndex, tongtienGiuongBHYT.Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("G" + rowIndex, "G" + rowIndex, (Convert.ToDouble(tongtiengiuong.Value) - tongtienGiuongBHYT.Value).ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "G" + rowIndex, "2. Đơn thuốc", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, true, 12);
                rowIndex += 1;
                var donThuoc = (from a in db.ChiTietDonThuocs.ToList()
                                join b in db.Thuocs on a.ThuocID equals b.Id into b1
                                from b in b1.DefaultIfEmpty()
                                where data.DonThuoc == null ? false: a.DonThuocID == data.DonThuoc.Id
                                select new
                                {
                                    Thuoc = b,
                                    ChiTiet = a
                                }).ToList();
                decimal? tongTienThuoc = 0;
                double? tongTienThuocBHYT = 0;
                foreach (var item in donThuoc)
                {
                    tongTienThuoc += item.Thuoc.Gia * item.ChiTiet.SoLuong;
                    double? tongThuoc = Convert.ToDouble(item.Thuoc.Gia * item.ChiTiet.SoLuong);
                    double? tongThuocBHYT = 0;
                    if (item.Thuoc.GiamGiaBHYT == EnumStatus.ACTIVE)
                    {
                        tongTienThuocBHYT += Convert.ToDouble(item.Thuoc.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                        tongThuocBHYT = Convert.ToDouble(item.Thuoc.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                    }

                    dynamic[] val = { item.Thuoc.Ten, item.Thuoc.DonVi, item.ChiTiet.SoLuong.Value, item.Thuoc.Gia.Value.ToString("#,###"), tongThuoc.Value.ToString("#,###"), tongThuocBHYT.Value.ToString("#,###"), (Convert.ToDouble(tongThuoc) - tongThuocBHYT).Value.ToString("#,###") };



                    ws.AddValue("A" + rowIndex, "G" + rowIndex, val, fontSizeNoiDung, false, XlHAlign.xlHAlignLeft, false);
                    rowIndex += 1;
                }
                ws.AddValue("A" + rowIndex, "D" + rowIndex, "Tổng cộng (2)", fontSizeTenTruong, true, XlHAlign.xlHAlignRight, true, 12);
                ws.AddValue("E" + rowIndex, "E" + rowIndex, tongTienThuoc.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("F" + rowIndex, "F" + rowIndex, tongTienThuocBHYT.Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("G" + rowIndex, "G" + rowIndex, (tongTienThuoc - Convert.ToDecimal(tongTienThuocBHYT)).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "G" + rowIndex, "3. Xét nghiệm cận lâm sàng (3)", fontSizeTenTruong, true, XlHAlign.xlHAlignLeft, true, 12);
                rowIndex += 1;

                var canlamsang = (from a in db.ChiTietSuDungDichVus.ToList()
                                  join b in db.SuDungDichVus on a.SuDungDichVuID equals b.Id into b1
                                  from b in b1.DefaultIfEmpty()
                                  join c in db.DichVus on a.DichVuID equals c.Id
                                  where b.NhapVienID == data.NhapVien.Id
                                  select new
                                  {
                                      DichVu = c,
                                      SuDungDichVus = b,
                                      ChiTiet = a
                                  }).ToList();
                decimal? tongTienDichVu = 0;
                double? tongTienDichVuBHYT = 0;
                foreach (var item in canlamsang)
                {
                    tongTienDichVu += item.DichVu.Gia * item.ChiTiet.SoLuong;
                    double? tongDichVu = Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong);
                    double? tongDichVuBHYT = 0;
                    if (item.DichVu.ApDungBHYT == EnumStatus.ACTIVE)
                    {
                        tongTienDichVuBHYT += Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                        tongDichVuBHYT = Convert.ToDouble(item.DichVu.Gia * item.ChiTiet.SoLuong) * data.NguoiDung.TLMienGiam / 100;
                    }
                    dynamic[] val = { item.DichVu.Ten, "lần", item.ChiTiet.SoLuong.Value, item.DichVu.Gia.Value.ToString("#,###"), tongDichVu.Value.ToString("#,###"), tongDichVuBHYT.Value.ToString("#,###"), (Convert.ToDouble(tongDichVu) - tongDichVuBHYT).Value.ToString("#,###") };
                    ws.AddValue("A" + rowIndex, "G" + rowIndex, val, fontSizeNoiDung, false, XlHAlign.xlHAlignLeft, false);
                    rowIndex += 1;
                }
                ws.AddValue("A" + rowIndex, "D" + rowIndex, "Tổng cộng (3)", fontSizeTenTruong, true, XlHAlign.xlHAlignRight, true, 12);
                ws.AddValue("E" + rowIndex, "E" + rowIndex, tongTienDichVu.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("F" + rowIndex, "F" + rowIndex, tongTienDichVuBHYT.Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("G" + rowIndex, "G" + rowIndex, (tongTienDichVu - Convert.ToDecimal(tongTienDichVuBHYT)).Value.ToString("#,###"), fontSizeTenTruong, false, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;

                ws.get_Range("A" + rowStart, "G" + rowIndex).SetBorderAround();

                // total
                var total = tongtiengiuong + tongTienDichVu + tongTienThuoc;
                var totalBHYT = tongtienGiuongBHYT + tongTienDichVuBHYT + tongTienThuocBHYT;
                ws.AddValue("A" + rowIndex, "C" + rowIndex, "Tổng số tiền (1) (2) (3) :", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("D" + rowIndex, "E" + rowIndex, (total.Value == 0 || total == null) ? "0" : total.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "C" + rowIndex, "BHYT chi trả (1) (2) (3) :", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("D" + rowIndex, "E" + rowIndex, (totalBHYT.Value == 0 || totalBHYT == null) ? "0" : totalBHYT.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);

                rowIndex += 1;
                var totalMoney = db.ChiTietChiPhiNhapViens.Where(x => x.NhapVienID == data.NhapVien.Id).Sum(x => x.SoTien);
                totalMoney = totalMoney == null ? 0 : totalMoney;
                ws.AddValue("A" + rowIndex, "C" + rowIndex, "Số tiền đã nộp :", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("D" + rowIndex, "E" + rowIndex, totalMoney.Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);

                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "C" + rowIndex, "Người bệnh chi trả (1) (2) (3) :", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("D" + rowIndex, "E" + rowIndex, (Convert.ToDouble(total) - totalBHYT - totalMoney).Value.ToString("#,###"), fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);

                // SIGN
                rowIndex += 1;
                ws.AddValue("E" + rowIndex, "G" + rowIndex, "Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year, fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "B" + rowIndex, "NGƯỜI LẬP", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("C" + rowIndex, "D" + rowIndex, "NGƯỜI BỆNH", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("F" + rowIndex, "G" + rowIndex, "KẾ TOÁN", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                rowIndex += 1;
                ws.AddValue("A" + rowIndex, "B" + rowIndex, "( ký ghi rõ họ tên )", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("C" + rowIndex, "D" + rowIndex, "( ký ghi rõ họ tên )", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);
                ws.AddValue("F" + rowIndex, "G" + rowIndex, "(ký ghi rõ họ tên)", fontSizeTenTruong, true, XlHAlign.xlHAlignCenter, true, 12);

                string fileName1 = "\\Export\\HD" + DateTime.Now.ToString("ddMMyyyy") + data.NhapVien.MaBenhAn + ".pdf";
                string filePath1 = HttpContext.Server.MapPath("~" + fileName1);


                //Save
                wb.SaveAs(filePath);
                wb.SaveAs(filePath, XlFileFormat.xlOpenXMLWorkbook, missing, missing, false, false
                    , XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlUserResolution
                    , true, missing, missing, missing);
                wb.Saved = true;
                wb.Close(true, missing, missing);
                ExportWorkbookToPdf(filePath, filePath1);
                var excelApp = new Application();
                excelApp.Visible = true;
                excelApp.Workbooks.Open(filePath);

                //wb.Close();
                //thoát và thu hồi bộ nhớ cho COM
                ws.ReleaseObject();
                wb.ReleaseObject();
                return Json(new { kq = "ok", data = filePath1, msg = "Thành công!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { kq = "err", msg = "Đã xảy ra lỗi khi lưu dữ liệu. Kiểm tra File" }, JsonRequestBehavior.AllowGet);
            }
            finally
            {
                if (xlApp != null)
                {
                    xlApp.Quit();
                }
                xlApp.ReleaseObject();
            }
        }


        [Route("noi-tru/export-pdf-excel")]
        public bool ExportWorkbookToPdf(string workbookPath, string outputPath)
        {
            // If either required string is null or empty, stop and bail out
            if (string.IsNullOrEmpty(workbookPath) || string.IsNullOrEmpty(outputPath))
            {
                return false;
            }

            // Create COM Objects
            Microsoft.Office.Interop.Excel.Application excelApplication;
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook;

            // Create new instance of Excel
            excelApplication = new Microsoft.Office.Interop.Excel.Application();

            // Make the process invisible to the user
            excelApplication.ScreenUpdating = false;

            // Make the process silent
            excelApplication.DisplayAlerts = false;

            // Open the workbook that you wish to export to PDF
            excelWorkbook = excelApplication.Workbooks.Open(workbookPath);

            // If the workbook failed to open, stop, clean up, and bail out
            if (excelWorkbook == null)
            {
                excelApplication.Quit();

                excelApplication = null;
                excelWorkbook = null;

                return false;
            }

            var exportSuccessful = true;
            try
            {
                // Call Excel's native export function (valid in Office 2007 and Office 2010, AFAIK)
                excelWorkbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, outputPath);
            }
            catch (System.Exception)
            {
                // Mark the export as failed for the return value...
                exportSuccessful = false;

                // Do something with any exceptions here, if you wish...
                // MessageBox.Show...        
            }
            finally
            {
                // Close the workbook, quit the Excel, and clean up regardless of the results...
                excelWorkbook.Close();
                excelApplication.Quit();

                excelApplication = null;
                excelWorkbook = null;
            }

            // You can use the following method to automatically open the PDF after export if you wish
            // Make sure that the file actually exists first...
            if (System.IO.File.Exists(outputPath))
            {
                System.Diagnostics.Process.Start(outputPath);
            }

            return exportSuccessful;
        }

    }
}