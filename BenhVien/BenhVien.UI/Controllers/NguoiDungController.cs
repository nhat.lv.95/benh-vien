﻿using BASICAUTHORIZE.AGRICH;
using BenhVien.MODEL;
using BenhVien.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BenhVien.UI.Controllers
{
    [RouteArea("QuanTri", AreaPrefix = "admin")]
    [Route("{action}")]
    public class NguoiDungController : Controller
    {
        public BenhVienEntities db = new BenhVienEntities();
        [Route("nguoi-dung/main-page")]
        public ActionResult MainPage()
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });
            return View();
        }
        [Route("nguoi-dung/list")]
        public ActionResult List(string keyword = "", int? status = null, int sotrang = 1, int? type = EnumUserType.BAC_SI, int tongsodong = 5)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            if (keyword != "")
                keyword = keyword.BoDauTiengViet().ToLower();

            var list = (from a in db.NguoiDungs.ToList()
                        where (keyword == "" || (keyword != "" && (a.HoTen.BoDauTiengViet().ToLower().Contains(keyword) ||(!string.IsNullOrEmpty(a.DiaChi)&& a.DiaChi.BoDauTiengViet().ToLower().Contains(keyword)) || (a.SDT != null && a.SDT.ToLower().Contains(keyword)))))
                        && (a != null &&
                        a.LoaiNguoiDung != EnumUserType.ADMIN
                        && a.LoaiNguoiDung != EnumUserType.SUB_ADMIN
                        && (status == null ? true : a.TrangThaiID == status)
                        && a.LoaiNguoiDung == type
                        )

                        select new UserInfo()
                        {
                            User = a,
                        }).OrderBy(x => x.User.Id);

            int tongso = list.Count();

            sotrang = sotrang <= 0 ? 1 : sotrang;
            tongsodong = tongsodong <= 0 ? 10 : tongsodong;
            int tongsotrang = tongso % tongsodong > 0 ? tongso / tongsodong + 1 : tongso / tongsodong;
            tongsotrang = tongsotrang <= 0 ? 1 : tongsotrang;
            sotrang = sotrang > tongsotrang ? tongsotrang - 1 : sotrang - 1;
            ViewBag.sotrang = sotrang + 1;
            ViewBag.tongsotrang = tongsotrang;
            ViewBag.tongso = tongso;
            return PartialView(list == null ? list : list.Skip(sotrang * tongsodong).Take(tongsodong));
        }
        [Route("nguoi-dung/update")]
        public ActionResult Update(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var user = db.NguoiDungs.FirstOrDefault(x => x.Id == id);
            string cbxProvince = "<option value=\"\">Chọn tỉnh/ thành phố</option>";
            var province = db.Provinces.OrderByDescending(x => x.STT).ToList();
            foreach (var item in province)
            {
                cbxProvince += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Name, (user != null && user.ThanhPhoID == item.Id) ? "selected" : "");
            }
            ViewBag.cbxProvince = cbxProvince;

            string cbxKhoa = "<option value=\"\">Chọn khoa</option>";
            var khoa = db.Khoas.Where(x => x.TrangThaiID == EnumStatus.ACTIVE).OrderByDescending(x=>x.Id).ToList();
            foreach (var item in khoa)
            {
                cbxKhoa += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Ten, (user != null && user.KhoaID == item.Id) ? "selected" : "");
            }
            ViewBag.cbxKhoa = cbxKhoa;
            return PartialView(user);
        }
        [Route("nguoi-dung/update")]
        [HttpPost, ValidateInput(false)]
        public ActionResult Update(NguoiDung user, HttpPostedFileBase _Avatar = null, string NgaySinh = "")
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });



            if (!user.Email.checkEmail())
                return Json(new { kq = "err", msg = "Email không đúng !" }, JsonRequestBehavior.AllowGet);
            if (!user.SDT.checkSoDienThoai())
                return Json(new { kq = "err", msg = "Số điện thoại không hợp lệ !" }, JsonRequestBehavior.AllowGet);
            DateTime? dateOfBirth = NgaySinh.ToDateTime();
            var appConfigs = db.AppConfigs.FirstOrDefault();
            if (user.Id == 0)
            {
                // tao moi
                var checkExist = db.NguoiDungs.Where(x => x.TenDangNhap == user.TenDangNhap);
                if (checkExist.Count() > 0)
                    return Json(new { kq = "err", msg = "Thông tin người dùng đã tồn tại!" }, JsonRequestBehavior.AllowGet);
                if (_Avatar != null)
                {
                    string rootPathImage = string.Format("~/Files/nguoi-dung/avatar/{0}", DateTime.Now.ToString("ddMMyyyy"));
                    string filePathImage = System.IO.Path.Combine(Request.MapPath(rootPathImage));
                    string[] fileImage = _Avatar.uploadFile(rootPathImage, filePathImage);
                    user.Avatar = fileImage[1];
                }
                var matKhau = user.MatKhau.Encode();
                user.Code = Functions.CreateCode();
                user.MatKhau = matKhau;
                user.NgaySinh = dateOfBirth;
                db.NguoiDungs.Add(user);
                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // up date
                var userOld = db.NguoiDungs.FirstOrDefault(x => x.Id == user.Id);
                if (userOld == null)
                    return Json(new { kq = "err", msg = "Thông tin không xác định!" }, JsonRequestBehavior.AllowGet);
                var checkExist = db.NguoiDungs.Count(x => x.Id != user.Id && (x.TenDangNhap == user.TenDangNhap));
                if (checkExist > 0)
                    return Json(new { kq = "err", msg = "Thông tin người dùng đã tồn tại rồi!" }, JsonRequestBehavior.AllowGet);


                if (_Avatar != null)
                {
                    string rootPathImage = string.Format("~/Files/user/avatar/{0}/{1}", user.Code, DateTime.Now.ToString("ddMMyyyy"));
                    string filePathImage = System.IO.Path.Combine(Request.MapPath(rootPathImage));
                    string[] fileImage = _Avatar.uploadFile(rootPathImage, filePathImage);
                    userOld.Avatar = fileImage[1];
                }
                userOld.HoTen = user.HoTen;
                userOld.TrangThaiID = user.TrangThaiID;
                userOld.SDT = user.SDT;
                userOld.Email = user.Email;
                userOld.NgaySinh = dateOfBirth;

                userOld.ThanhPhoID = user.ThanhPhoID;
                userOld.QuanHuyenID = user.QuanHuyenID;
                userOld.PhuongXaID = user.PhuongXaID;
                userOld.GioiTinh = user.GioiTinh;
                userOld.KhoaID = user.KhoaID;
                userOld.DanToc = user.DanToc;
                userOld.DiaChi = user.DiaChi;
                userOld.Mota = user.Mota;
                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
        }



        [Route("nguoi-dung/benh-nhan-update")]
        public ActionResult BenhNhan_Update(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var user = db.NguoiDungs.FirstOrDefault(x => x.Id == id);
            string cbxProvince = "<option value=\"\">Chọn tỉnh/ thành phố</option>";
            var province = db.Provinces.OrderByDescending(x => x.STT).ToList();
            foreach (var item in province)
            {
                cbxProvince += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Name, (user != null && user.ThanhPhoID == item.Id) ? "selected" : "");
            }
            ViewBag.cbxProvince = cbxProvince;
            string cbxKhoa = "<option value=\"\">Chọn khoa</option>";
            var khoa = db.Khoas.Where(x => x.TrangThaiID == EnumStatus.ACTIVE).OrderByDescending(x => x.Id).ToList();
            foreach (var item in khoa)
            {
                cbxKhoa += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Ten, (user != null && user.KhoaID == item.Id) ? "selected" : "");
            }
            ViewBag.cbxKhoa = cbxKhoa;
            return PartialView(user);
        }
        [Route("nguoi-dung/benh-nhan-update")]
        [HttpPost, ValidateInput(false)]
        public ActionResult BenhNhan_Update(NguoiDung user, HttpPostedFileBase _Avatar = null, string NgaySinh = "", string NgayApDungBHYT ="", string NgayHetHanBHYT="")
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });



            if (!user.Email.checkEmail())
                return Json(new { kq = "err", msg = "Email không đúng !" }, JsonRequestBehavior.AllowGet);
            if (!user.SDT.checkSoDienThoai())
                return Json(new { kq = "err", msg = "Số điện thoại không hợp lệ !" }, JsonRequestBehavior.AllowGet);
            DateTime? dateOfBirth = NgaySinh.ToDateTime();
            DateTime? ngayApDung = NgayApDungBHYT.ToDateTime();
            DateTime? ngayHetHan = NgayHetHanBHYT.ToDateTime();
            var appConfigs = db.AppConfigs.FirstOrDefault();
            if (user.Id == 0)
            {
                // tao moi
                //var checkExist = db.NguoiDungs.Where(x => x.TenDangNhap == user.TenDangNhap);
                //if (checkExist.Count() > 0)
                //    return Json(new { kq = "err", msg = "Thông tin người dùng đã tồn tại!" }, JsonRequestBehavior.AllowGet);
                if (_Avatar != null)
                {
                    string rootPathImage = string.Format("~/Files/nguoi-dung/avatar/{0}", DateTime.Now.ToString("ddMMyyyy"));
                    string filePathImage = System.IO.Path.Combine(Request.MapPath(rootPathImage));
                    string[] fileImage = _Avatar.uploadFile(rootPathImage, filePathImage);
                    user.Avatar = fileImage[1];
                }
                user.Code = Functions.CreateCode();
                user.NgayApDungBHYT = ngayApDung;
                user.NgayHetHanBHYT = ngayHetHan;
                user.NgaySinh = dateOfBirth;
                db.NguoiDungs.Add(user);
                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // up date
                var userOld = db.NguoiDungs.FirstOrDefault(x => x.Id == user.Id);
                if (userOld == null)
                    return Json(new { kq = "err", msg = "Thông tin không xác định!" }, JsonRequestBehavior.AllowGet);
                var checkExist = db.NguoiDungs.Count(x => x.Id != user.Id && (x.TenDangNhap == user.TenDangNhap));
                if (checkExist > 0)
                    return Json(new { kq = "err", msg = "Thông tin người dùng đã tồn tại rồi!" }, JsonRequestBehavior.AllowGet);


                if (_Avatar != null)
                {
                    string rootPathImage = string.Format("~/Files/user/avatar/{0}/{1}", user.Code, DateTime.Now.ToString("ddMMyyyy"));
                    string filePathImage = System.IO.Path.Combine(Request.MapPath(rootPathImage));
                    string[] fileImage = _Avatar.uploadFile(rootPathImage, filePathImage);
                    userOld.Avatar = fileImage[1];
                }
                userOld.HoTen = user.HoTen;
                userOld.TrangThaiID = user.TrangThaiID;
                userOld.SDT = user.SDT;
                userOld.Email = user.Email;
                userOld.NgaySinh = dateOfBirth;
                userOld.ThanhPhoID = user.ThanhPhoID;
                userOld.QuanHuyenID = user.QuanHuyenID;
                userOld.PhuongXaID = user.PhuongXaID;

                userOld.NgayApDungBHYT = ngayApDung;
                userOld.NgayHetHanBHYT = ngayHetHan;
                userOld.GioiTinh = user.GioiTinh;
                userOld.KhoaID = user.KhoaID;
                userOld.DanToc = user.DanToc;
                userOld.CMND = user.CMND;
                userOld.NgheNghiep = user.NgheNghiep;
                userOld.SoBHYT = user.SoBHYT;
               

                userOld.DiaChi = user.DiaChi;
                userOld.Mota = user.Mota;

                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
        }



        [Route("nguoi-dung/delete")]
        public ActionResult Delete(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var user = db.NguoiDungs.FirstOrDefault(x => x.Id == id);
            if (user == null)
                return Json(new { kq = "err", msg = "Không xác định!" }, JsonRequestBehavior.AllowGet);

            if (user.Id == nd_dv.User.Id)
                return Json(new { kq = "err", msg = "Không thể xóa tài khoản đang đăng nhập!" }, JsonRequestBehavior.AllowGet);
            
            db.NguoiDungs.Remove(user);
            db.SaveChanges();
            return Json(new { kq = "ok", msg = "Thành công!" }, JsonRequestBehavior.AllowGet);
        }

        [Route("nguoi-dung/change-status")]
        public ActionResult Change_Status(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var user = db.NguoiDungs.FirstOrDefault(x => x.Id == id);
            if (user == null)
                return Json(new { kq = "err", msg = "Không xác định!" }, JsonRequestBehavior.AllowGet);
            if (user.Id == nd_dv.User.Id)
                return Json(new { kq = "err", msg = "Không thể khóa tài khoản đang đăng nhập!" }, JsonRequestBehavior.AllowGet);
            if (user.LoaiNguoiDung == EnumUserType.ADMIN || user.LoaiNguoiDung == EnumUserType.SUB_ADMIN)
                return Json(new { kq = "err", msg = "Không thể khóa tài khoản quản trị!" }, JsonRequestBehavior.AllowGet);
            if (user.TrangThaiID == EnumStatus.ACTIVE)
                user.TrangThaiID = EnumStatus.INACTIVE;
            else
                user.TrangThaiID = EnumStatus.ACTIVE;
            db.SaveChanges();
            return Json(new { kq = "ok", data = user.TrangThaiID, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }
        [Route("nguoi-dung/info")]
        public ActionResult Info(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var user = (from a in db.NguoiDungs.ToList()
                        where id == a.Id
                        select new UserInfo()
                        {
                            User = a
                        }).FirstOrDefault();
            string avatar = "";
            if (user.User != null)
            {
                avatar = user.User.Avatar;
            }
            ViewBag.Avatar = avatar;

            string cbxProvince = "<option value=\"\">Chọn tỉnh/ thành phố</option>";
            var province = db.Provinces.OrderByDescending(x => x.STT).ToList();
            foreach (var item in province)
            {
                cbxProvince += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Name, (user != null && user.User.ThanhPhoID == item.Id) ? "selected" : "");
            }
            ViewBag.cbxProvince = cbxProvince;
            if (user == null)
                user = new UserInfo();
            return View(user);
        }
        [Route("nguoi-dung/info")]
        [HttpPost, ValidateInput(false)]
        public ActionResult Info(NguoiDung user, HttpPostedFileBase _Avatar = null, string BirthDay = "")
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            DateTime? dateOfBirth = BirthDay.ToDateTime();
            var userOld = db.NguoiDungs.FirstOrDefault(x => x.Id == user.Id);
            if (userOld == null)
                return Json(new { kq = "err", msg = "Thông tin không xác định!" }, JsonRequestBehavior.AllowGet);

            var checkExist = db.NguoiDungs.Count(x => x.Id != user.Id && (string.IsNullOrEmpty(user.Email) ? true : x.Email == user.Email || string.IsNullOrEmpty(user.SDT) ? true : x.SDT == user.SDT));
            if (checkExist > 0)
                return Json(new { kq = "err", msg = "Thông tin người dùng đã tồn tại rồi!" }, JsonRequestBehavior.AllowGet);
            if (_Avatar != null)
            {
                string rootPathImage = string.Format("~/Files/nguoi-dung/avatar/{0}", DateTime.Now.ToString("ddMMyyyy"));
                string filePathImage = System.IO.Path.Combine(Request.MapPath(rootPathImage));
                string[] fileImage = _Avatar.uploadFile(rootPathImage, filePathImage);
                userOld.Avatar = fileImage[1];

            }
            userOld.DiaChi = user.DiaChi;
            userOld.SDT = user.SDT;
            userOld.NgaySinh = dateOfBirth;
            userOld.Email = user.Email;
            userOld.ThanhPhoID = user.ThanhPhoID;
            userOld.QuanHuyenID = user.QuanHuyenID;
            userOld.PhuongXaID = user.PhuongXaID;
            db.SaveChanges();
            return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }
        [Route("nguoi-dung/reset-password")]
        public ActionResult ResetPassword(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var user = db.NguoiDungs.FirstOrDefault(x => x.Id == id);
            if (user == null)
                return Json(new { kq = "err", msg = "Thông tin không xác định!" }, JsonRequestBehavior.AllowGet);

            var newPassword = Functions.CreateCode6Char();
            user.MatKhau = newPassword.Encode();
            db.SaveChanges();
            return Json(new { kq = "ok", data = newPassword, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }


        [Route("nguoi-dung/get-district-by-province")]
        public ActionResult GetDistrictByProvince(int? id = null, int? select = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });
            var district = db.Disctricts.Where(x => x.ProvinceID == id).ToList();
            string cbx = "<option value=\"\">Chọn quận/ huyện</option>";
            foreach (var item in district)
            {
                cbx += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Name, (select != null && select == item.Id) ? "selected" : "");
            }
            return Json(new { kq = "ok", data = cbx, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }

        [Route("nguoi-dung/get-comune-by-district")]
        public ActionResult GetComuneByDistrict(int? id = null, int? select = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });
            var comune = db.Comunes.Where(x => x.DisctrictsID == id).ToList();
            string cbx = "<option value=\"\">Chọn phường/ xã</option>";
            foreach (var item in comune)
            {
                cbx += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Name, (select != null && select == item.Id) ? "selected" : "");
            }
            return Json(new { kq = "ok", data = cbx, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }
    }
}