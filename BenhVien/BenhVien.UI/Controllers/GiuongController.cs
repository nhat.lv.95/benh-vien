﻿using BASICAUTHORIZE.AGRICH;
using BenhVien.MODEL;
using BenhVien.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BenhVien.UI.Controllers
{
    [RouteArea("QuanTri", AreaPrefix = "admin")]
    [Route("{action}")]
    public class GiuongController : Controller
    {
        public BenhVienEntities db = new BenhVienEntities();
        [Route("giuong/main-page")]
        public ActionResult MainPage()
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });
            return View();
        }
        [Route("giuong/list")]
        public ActionResult List(string keyword = "", int? status = EnumStatus.ACTIVE, int sotrang = 1, int tongsodong = 5)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            if (keyword != "")
                keyword = keyword.BoDauTiengViet().ToLower();

            var list = (from a in db.Giuongs.ToList()
                        join b in db.Phongs on a.PhongID equals b.Id into b1
                        from b in b1.DefaultIfEmpty()
                        where (keyword == "" || (keyword != "" && (a.Ten.BoDauTiengViet().ToLower().Contains(keyword))
                        )
                        )
                        && (status == null ? true : a.TrangThaiID == status)
                        select new ThongTinGiuong
                        {
                            Giuong = a,
                            Phong = b
                        }).OrderBy(x => x.Giuong.Id);

            int tongso = list.Count();

            sotrang = sotrang <= 0 ? 1 : sotrang;
            tongsodong = tongsodong <= 0 ? 10 : tongsodong;
            int tongsotrang = tongso % tongsodong > 0 ? tongso / tongsodong + 1 : tongso / tongsodong;
            tongsotrang = tongsotrang <= 0 ? 1 : tongsotrang;
            sotrang = sotrang > tongsotrang ? tongsotrang - 1 : sotrang - 1;
            ViewBag.sotrang = sotrang + 1;
            ViewBag.tongsotrang = tongsotrang;
            ViewBag.tongso = tongso;
            return PartialView(list == null ? list : list.Skip(sotrang * tongsodong).Take(tongsodong));
        }
        [Route("giuong/update")]
        public ActionResult Update(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });
            var giuong = db.Giuongs.FirstOrDefault(x => x.Id == id);

            var phong = db.Phongs.Where(x => x.TrangThaiID == EnumStatus.ACTIVE && x.LoaiPhong == EnumLoaiPhong.PHONG_BENH).ToList();
            string cbx = "<option value=\"\">Chọn phòng</option>";
            foreach (var item in phong)
            {
                cbx += string.Format("<option value=\"{0}\" {2}>{1}</option>", item.Id, item.Ten, (giuong != null && giuong.PhongID == item.Id) ? "selected" : "");
            }
            ViewBag.cbxPhong = cbx;

            return PartialView(giuong);
        }
        [Route("giuong/update")]
        [HttpPost, ValidateInput(false)]
        public ActionResult Update(Giuong giuong)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });
            if (giuong.Id == 0)
            {
                db.Giuongs.Add(giuong);
                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // up date
                var old = db.Giuongs.FirstOrDefault(x => x.Id == giuong.Id);
                if (old == null)
                    return Json(new { kq = "err", msg = "Thông tin không xác định!" }, JsonRequestBehavior.AllowGet);
                old.Ten = giuong.Ten;
                old.PhongID = giuong.PhongID;
                old.TrangThaiID = giuong.TrangThaiID;
                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
        }


        [Route("giuong/delete")]
        public ActionResult Delete(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var giuong = db.Giuongs.FirstOrDefault(x => x.Id == id);
            if (giuong == null)
                return Json(new { kq = "err", msg = "Không xác định!" }, JsonRequestBehavior.AllowGet);

            db.Giuongs.Remove(giuong);
            db.SaveChanges();
            return Json(new { kq = "ok", msg = "Thành công!" }, JsonRequestBehavior.AllowGet);
        }

        [Route("giuong/change-status")]
        public ActionResult Change_Status(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var giuong = db.Giuongs.FirstOrDefault(x => x.Id == id);
            if (giuong == null)
                return Json(new { kq = "err", msg = "Không xác định!" }, JsonRequestBehavior.AllowGet);

            if (giuong.TrangThaiID == EnumStatus.ACTIVE)
                giuong.TrangThaiID = EnumStatus.INACTIVE;
            else
                giuong.TrangThaiID = EnumStatus.ACTIVE;
            db.SaveChanges();
            return Json(new { kq = "ok", data = giuong.TrangThaiID, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }
    }
}