﻿using BASICAUTHORIZE.AGRICH;
using BenhVien.MODEL;
using BenhVien.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BenhVien.UI.Controllers
{
    [RouteArea("QuanTri", AreaPrefix = "admin")]
    [Route("{action}")]
    public class ThuocController : Controller
    {
        public BenhVienEntities db = new BenhVienEntities();
        [Route("thuoc/main-page")]
        public ActionResult MainPage()
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });
            return View();
        }
        [Route("thuoc/list")]
        public ActionResult List(string keyword = "", int? status = EnumStatus.ACTIVE, int sotrang = 1, int tongsodong = 5)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            if (keyword != "")
                keyword = keyword.BoDauTiengViet().ToLower();
            var list = (from a in db.Thuocs.ToList()
                        where (keyword == "" || (keyword != "" && (a.Ten.BoDauTiengViet().ToLower().Contains(keyword))
                        || (a.MoTa.BoDauTiengViet().ToLower().Contains(keyword))
                        || (a.CongTy.BoDauTiengViet().ToLower().Contains(keyword))
                         || (a.DonVi.BoDauTiengViet().ToLower().Contains(keyword))
                        )
                        )
                        && (status == null ? true : a.TrangThaiID == status)
                        select a).OrderBy(x => x.Id);

            int tongso = list.Count();

            sotrang = sotrang <= 0 ? 1 : sotrang;
            tongsodong = tongsodong <= 0 ? 10 : tongsodong;
            int tongsotrang = tongso % tongsodong > 0 ? tongso / tongsodong + 1 : tongso / tongsodong;
            tongsotrang = tongsotrang <= 0 ? 1 : tongsotrang;
            sotrang = sotrang > tongsotrang ? tongsotrang - 1 : sotrang - 1;
            ViewBag.sotrang = sotrang + 1;
            ViewBag.tongsotrang = tongsotrang;
            ViewBag.tongso = tongso;
            return PartialView(list == null ? list : list.Skip(sotrang * tongsodong).Take(tongsodong));
        }
        [Route("thuoc/update")]
        public ActionResult Update(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var thuoc = db.Thuocs.FirstOrDefault(x => x.Id == id);
            return PartialView(thuoc);
        }
        [Route("thuoc/update")]
        [HttpPost, ValidateInput(false)]
        public ActionResult Update(Thuoc thuoc, HttpPostedFileBase _Logo = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

     
            
            if (thuoc.Id == 0)
            {

                if (_Logo != null)
                {
                    string rootPathImage = string.Format("~/Files/enterprise/logo/{0}", DateTime.Now.ToString("ddMMyyyy"));
                    string filePathImage = System.IO.Path.Combine(Request.MapPath(rootPathImage));
                    string[] fileImage = _Logo.uploadFile(rootPathImage, filePathImage);
                    thuoc.AnhDaiDien = fileImage[1];
                }
                db.Thuocs.Add(thuoc);
                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // up date
                var old = db.Thuocs.FirstOrDefault(x => x.Id == thuoc.Id);
                if (old == null)
                    return Json(new { kq = "err", msg = "Thông tin không xác định!" }, JsonRequestBehavior.AllowGet);

                if (_Logo != null)
                {
                    string rootPathImage = string.Format("~/Files/enterprise/logo/{0}", DateTime.Now.ToString("ddMMyyyy"));
                    string filePathImage = System.IO.Path.Combine(Request.MapPath(rootPathImage));
                    string[] fileImage = _Logo.uploadFile(rootPathImage, filePathImage);
                    thuoc.AnhDaiDien = fileImage[1];
                }
                old.Ten = thuoc.Ten;
                old.CongTy = thuoc.CongTy;
                old.DonVi = thuoc.DonVi;
                old.Gia = thuoc.Gia;
                old.GiamGiaBHYT = thuoc.GiamGiaBHYT;
                old.MoTa = thuoc.MoTa;
                old.TrangThaiID = thuoc.TrangThaiID;
                db.SaveChanges();
                return Json(new { kq = "ok", msg = "Success!" }, JsonRequestBehavior.AllowGet);
            }
        }


        [Route("thuoc/delete")]
        public ActionResult Delete(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var thuoc = db.Thuocs.FirstOrDefault(x => x.Id == id);
            if (thuoc == null)
                return Json(new { kq = "err", msg = "Không xác định!" }, JsonRequestBehavior.AllowGet);

            db.Thuocs.Remove(thuoc);
            db.SaveChanges();
            return Json(new { kq = "ok", msg = "Thành công!" }, JsonRequestBehavior.AllowGet);
        }

        [Route("thuoc/change-status")]
        public ActionResult Change_Status(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var thuoc = db.Thuocs.FirstOrDefault(x => x.Id == id);
            if (thuoc == null)
                return Json(new { kq = "err", msg = "Không xác định!" }, JsonRequestBehavior.AllowGet);

            if (thuoc.TrangThaiID == EnumStatus.ACTIVE)
                thuoc.TrangThaiID = EnumStatus.INACTIVE;
            else
                thuoc.TrangThaiID = EnumStatus.ACTIVE;
            db.SaveChanges();
            return Json(new { kq = "ok", data = thuoc.TrangThaiID, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }

        [Route("thuoc/change-bhyt")]
        public ActionResult ChangeBHYT(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var thuoc = db.Thuocs.FirstOrDefault(x => x.Id == id);
            if (thuoc == null)
                return Json(new { kq = "err", msg = "Không xác định!" }, JsonRequestBehavior.AllowGet);

            if (thuoc.GiamGiaBHYT == EnumStatus.ACTIVE)
                thuoc.GiamGiaBHYT = EnumStatus.INACTIVE;
            else
                thuoc.GiamGiaBHYT = EnumStatus.ACTIVE;
            db.SaveChanges();
            return Json(new { kq = "ok", data = thuoc.GiamGiaBHYT, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }
    }
}