﻿using BASICAUTHORIZE.AGRICH;
using BenhVien.MODEL;
using BenhVien.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BenhVien.UI.Controllers
{
    [RouteArea("QuanTri", AreaPrefix = "admin")]
    [Route("{action}")]
    public class DonThuocController : Controller
    {
        public BenhVienEntities db = new BenhVienEntities();
        [Route("don-thuoc/main-page")]
        public ActionResult MainPage()
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });
            return View();
        }
        [Route("don-thuoc/list")]
        public ActionResult List(string keyword = "", int? status = EnumTrangThaiDonThuoc.CHUA_PHAT, string fromDate = "", string toDate = "", int sotrang = 1, int tongsodong = 5)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });
            DateTime? dateFrom = fromDate.ToDateTime();
            DateTime? dateTo = toDate.ToDateTime();
            if (keyword != "")
                keyword = keyword.BoDauTiengViet().ToLower();
            var data = (from a in db.DonThuocs.ToList()
                        join b in db.ChiTietDonThuocs on a.Id equals b.DonThuocID
                        join c in db.Thuocs on b.ThuocID equals c.Id into c1
                        from c in c1.DefaultIfEmpty()
                        join d in db.NguoiDungs on a.BacSiID equals d.Id into d1
                        from d in d1.DefaultIfEmpty()
                        join e in db.KhamBenhs on a.KhamBenhID equals e.Id into e1
                        from e in e1.DefaultIfEmpty()
                        join f in db.NhapViens on a.NhapVienID equals f.Id into f1
                        from f in f1.DefaultIfEmpty()
                        where b != null && (keyword == "" || (keyword != "" && (c.Ten.BoDauTiengViet().ToLower().Contains(keyword))
                        || (c.MoTa.BoDauTiengViet().ToLower().Contains(keyword))
                        || (c.CongTy.BoDauTiengViet().ToLower().Contains(keyword))
                         || (c.DonVi.BoDauTiengViet().ToLower().Contains(keyword))
                         || (a.MaDonThuoc.BoDauTiengViet().ToLower().Contains(keyword))
                        )
                        )
                        && (status == null ? true : a.TrangThaiID == status)
                        select new
                        {
                            DonThuoc = a,
                            NguoiDung = d,
                            ChiTietDonThuoc = b,
                            Thuoc = c,
                            KhamBenh = e,
                            NhapVien = f,
                        }).Where(a => (string.IsNullOrEmpty(fromDate) ? true : a.DonThuoc.NgayPhatThuoc.Value.ToString("dd/MM/yyyy").ToDateTime() >= dateFrom)
                            && (string.IsNullOrEmpty(toDate) ? true : a.DonThuoc.NgayPhatThuoc.Value.ToString("dd/MM/yyyy").ToDateTime() <= dateTo))
                        .OrderByDescending(x => x.DonThuoc.NgayPhatThuoc).ToList();

            var list = data.GroupBy(x => x.DonThuoc.Id).Select(x => new DonThuocThongTin
            {

                DonThuoc = x.FirstOrDefault(y => y.DonThuoc.Id == x.Key).DonThuoc,
                NguoiDung = x.FirstOrDefault(y => y.DonThuoc.Id == x.Key).NguoiDung,
                NhapVien = x.FirstOrDefault(y => y.DonThuoc.Id == x.Key).NhapVien,
                KhamBenh = x.FirstOrDefault(y => y.DonThuoc.Id == x.Key).KhamBenh,

                ChiTiet = x.Where(y => y.DonThuoc.Id == x.Key).Select(t => new ThongTinChiTietDonThuoc
                {
                    ChiTietDonThuoc = t.ChiTietDonThuoc,
                    Thuoc = t.Thuoc
                }).ToList()
            }).ToList();

            int tongso = list.Count();

            sotrang = sotrang <= 0 ? 1 : sotrang;
            tongsodong = tongsodong <= 0 ? 10 : tongsodong;
            int tongsotrang = tongso % tongsodong > 0 ? tongso / tongsodong + 1 : tongso / tongsodong;
            tongsotrang = tongsotrang <= 0 ? 1 : tongsotrang;
            sotrang = sotrang > tongsotrang ? tongsotrang - 1 : sotrang - 1;
            ViewBag.sotrang = sotrang + 1;
            ViewBag.tongsotrang = tongsotrang;
            ViewBag.tongso = tongso;
            return PartialView(list == null ? list : list.Skip(sotrang * tongsodong).Take(tongsodong));
        }
        [Route("don-thuoc/change-status")]
        public ActionResult Change_Status(int? id = null)
        {
            UserInfo nd_dv = (UserInfo)Session["NguoiDung"];
            if (nd_dv == null || (nd_dv.User.LoaiNguoiDung != EnumUserType.ADMIN && nd_dv.User.LoaiNguoiDung != EnumUserType.SUB_ADMIN))
                return RedirectToAction("Index", "Home", new { area = "" });

            var don = db.DonThuocs.FirstOrDefault(x => x.Id == id);
            if (don == null)
                return Json(new { kq = "err", msg = "Không xác định!" }, JsonRequestBehavior.AllowGet);

            if (don.TrangThaiID == EnumTrangThaiDonThuoc.CHUA_PHAT)
                don.TrangThaiID = EnumTrangThaiDonThuoc.DA_PHAT;
            else
                don.TrangThaiID = EnumTrangThaiDonThuoc.DA_PHAT;
            db.SaveChanges();
            return Json(new { kq = "ok", data = don.TrangThaiID, msg = "Success!" }, JsonRequestBehavior.AllowGet);
        }
    }
}