﻿using BenhVien.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BenhVien.UI.Models
{
    public class ThongTinDonThuoc
    {
        public DonThuoc DonThuoc { get; set; }
        public ChiTietDonThuoc ChiTietDonThuoc { get; set; }
    }
}