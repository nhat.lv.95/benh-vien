﻿using BenhVien.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BenhVien.UI.Models
{
    public class ThongTinNhapVien
    {
        public NguoiDung NguoiDung { get; set; }
        public NhapVien NhapVien { get; set; }
        public ChiTietNhapVien ChiTietNhapVien { get; set; }
        public Khoa Khoa { get; set; }
        public Phong Phong { get; set; }
        public Giuong Giuong { get; set; }
    }
}