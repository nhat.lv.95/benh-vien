﻿using BenhVien.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BenhVien.UI.Models
{
    public class ThongTinGiuong
    {
        public Phong Phong { get; set; }
        public Giuong Giuong { get; set; }
    }
}