﻿using BenhVien.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BenhVien.UI.Models
{
    public class DangKyKhamBenh
    {
        public NguoiDung NguoiDung { get; set; }
        public KhamBenh KhamBenh { get; set; }
        public ChiTietKhamBenh ChiTietKhamBenh { get; set; }
    }
    public class ThongTinKhamBenh
    {
        public NguoiDung NguoiDung { get; set; }
        public KhamBenh KhamBenh { get; set; }
        public ChiTietKhamBenh ChiTietKhamBenh { get; set; }
        public DonThuoc DonThuoc { get; set; }
        public SuDungDichVu DichVu { get; set; }
       
    }
}