﻿using BenhVien.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BenhVien.UI.Models
{
    public class SuDungDichVuThongTin
    {
        public SuDungDichVu SuDungDichVu { get; set; }
        public NguoiDung NguoiDung { get; set; }
        public NhapVien NhapVien { get; set; }
        public KhamBenh KhamBenh { get; set; }
        public List<ChiTietSuDungDichVuThongTin> ChiTiet { get; set; }
    }
    public class ChiTietSuDungDichVuThongTin
    {
        public DichVu DichVu { get; set; }
        public ChiTietSuDungDichVu ChiTietSuDungDichVu { get; set; }
    }
}