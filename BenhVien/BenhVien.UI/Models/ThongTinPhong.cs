﻿using BenhVien.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BenhVien.UI.Models
{
    public class ThongTinPhong
    {
        public Phong Phong { get; set; }
        public Khoa Khoa { get; set; }

    }
}