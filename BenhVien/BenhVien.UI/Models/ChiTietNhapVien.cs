﻿using BenhVien.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BenhVien.UI.Models
{
    public class ThongTinChiTietNhapVien
    {
        public NguoiDung NguoiDung { get; set; }
        public NhapVien NhapVien { get; set; }
        public SuDungDichVu DichVu { get; set; }
        public DonThuoc DonThuoc { get; set; }
        public ChiTietNhapVien ChiTietNhapVien { get; set; }
    }
}