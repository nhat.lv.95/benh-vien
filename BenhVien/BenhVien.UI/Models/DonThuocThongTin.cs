﻿using BenhVien.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BenhVien.UI.Models
{
    public class DonThuocThongTin
    {
        public DonThuoc DonThuoc { get; set; }
        public NguoiDung NguoiDung { get; set; }
        public KhamBenh KhamBenh { get; set; }
        public NhapVien NhapVien { get; set; }
        public List<ThongTinChiTietDonThuoc> ChiTiet { get; set; }
    }
    public class ThongTinChiTietDonThuoc
    {
        public Thuoc Thuoc { get; set; }
        public ChiTietDonThuoc ChiTietDonThuoc { get; set; }
    }
}