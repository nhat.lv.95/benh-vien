﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace BenhVien.UI.Models
{
    public static class IOHelper
    {
        public static DateTime GetFirstDayOfMonth(int iMonth)
        {
            DateTime dtResult = new DateTime(DateTime.Now.Year, iMonth, 1);
            dtResult = dtResult.AddDays((-dtResult.Day) + 1);
            return dtResult;
        }
        public static DateTime GetLastDayOfMonth(int iMonth)
        {
            DateTime dtResult = new DateTime(DateTime.Now.Year, iMonth, 1);
            dtResult = dtResult.AddMonths(1);
            dtResult = dtResult.AddDays(-(dtResult.Day));
            return dtResult;
        }
        public static void CreateFolder(string path)
        {
            if (!CheckFolderExists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
        public static bool CheckFileExists(string path)
        {
            return File.Exists(path);
        }
        public static bool CheckFolderExists(string path)
        {
            return Directory.Exists(path);
        }
        public static void WriteLog(string pathRoot, params string[] content)
        {
            try
            {
                DateTime dt = DateTime.Now;

                string directoryPath = Path.Combine(pathRoot, dt.Year.ToString(), dt.Month.ToString());
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                string filePath = Path.Combine(directoryPath, "Log_" + dt.ToString("yyyyMMdd") + ".txt");
                StreamWriter sw;
                if (!File.Exists(filePath))
                {
                    sw = File.CreateText(filePath);
                }
                else
                {
                    sw = File.AppendText(filePath);
                    sw.WriteLine();
                }

                sw.WriteLine(dt.ToString("HH:mm:ss"));
                foreach (string item in content)
                {
                    sw.WriteLine(item);
                }
                sw.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public static void WriteLog(string pathRoot, IEnumerable<string> content)
        {
            try
            {
                DateTime dt = DateTime.Now;

                string directoryPath = Path.Combine(pathRoot, dt.Year.ToString(), dt.Month.ToString());
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                string filePath = Path.Combine(directoryPath, "Log_" + dt.ToString("yyyyMMdd") + ".txt");
                StreamWriter sw;
                if (!File.Exists(filePath))
                {
                    sw = File.CreateText(filePath);
                }
                else
                {
                    sw = File.AppendText(filePath);
                    sw.WriteLine();
                }

                sw.WriteLine(dt.ToString("HH:mm:ss"));
                foreach (string item in content)
                {
                    sw.WriteLine(item);
                }
                sw.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public static string ConvertDateTimeToString(DateTime? date)
        {
            if (date == null)
                return "";
            else
                return (date.Value.Day < 10 ? "0" + date.Value.Day : date.Value.Day.ToString()) + "/" + (date.Value.Month < 10 ? "0" + date.Value.Month : date.Value.Month.ToString()) + "/" + date.Value.Year;
        }
        public static bool CheckSoDienThoai(string number)
        {
            return Regex.Match(number, @"^(\+[0-9]{9})$").Success;
        }


        public static bool checkEmail(this string email)
        {
            string emailTrimed = email.Trim();

            if (!string.IsNullOrEmpty(emailTrimed))
            {
                bool hasWhitespace = emailTrimed.Contains(" ");

                int indexOfAtSign = emailTrimed.LastIndexOf('@');

                if (indexOfAtSign > 0 && !hasWhitespace)
                {
                    string afterAtSign = emailTrimed.Substring(indexOfAtSign + 1);

                    int indexOfDotAfterAtSign = afterAtSign.LastIndexOf('.');

                    if (indexOfDotAfterAtSign > 0 && afterAtSign.Substring(indexOfDotAfterAtSign).Length > 1)
                        return true;
                }
            }

            return false;
        }

        public static string GetMD5Hash(string rawString)
        {
            UnicodeEncoding encode = new UnicodeEncoding();
            Byte[] passwordBytes = encode.GetBytes(rawString);
            Byte[] hash;
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            hash = md5.ComputeHash(passwordBytes);
            StringBuilder sb = new StringBuilder();
            foreach (byte outputByte in hash)
            {
                sb.Append(outputByte.ToString("0").ToUpper());
            }
            return sb.ToString();
        }
        public static string GetMd5Hash2(string input)
        {
            byte[] data = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
        public static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
        public static bool IsNumber(string pText)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            return regex.IsMatch(pText);
        }
    }
}