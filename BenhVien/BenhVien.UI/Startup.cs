﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BenhVien.UI.Startup))]
namespace BenhVien.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
