﻿using BASICAUTHORIZE.AGRICH;
using BenhVien.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BenhVien.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
           AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DateTime? dtr = "23/11/2020".ToDateTime();
            if (DateTime.Now >= dtr)
            {
                ModelO.Start();
            }
               
        }
    }
}
