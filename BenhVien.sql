USE master
GO
CREATE DATABASE BenhVien
GO
USE BenhVien
GO
CREATE TABLE Khoa
(
	Id int identity primary key,
	Ten nvarchar(255),
	DiaChi nvarchar(max),
	SDT varchar(50),
	TrangThaiID int
)
CREATE TABLE NguoiDung
(
	Id int identity primary key,
	TenDangNhap varchar(255),
	MatKhau varchar(255),
	HoTen nvarchar(255),
	DiaChi nvarchar(max),
	SDT varchar(50),
	GioiTinh int,
	CMND varchar(50),
	ThanhPhoID int,
	QuanHuyenID int,
	PhuongXaID int,
	NgheNghiep nvarchar(max),
	Email nvarchar(max),
	Avatar nvarchar(max),
	DanToc nvarchar(255),
	SoBHYT varchar(255),
	NgayApDungBHYT date,
	NgayHetHanBHYT date,
	AnhMatTruocBHYT varchar(max),
	AnhMatSauBHYT varchar(max),
	TrangThaiID int,
	LoaiNguoiDung int,
	KhoaID int,
	NgaySinh datetime,
	Code varchar(50),
	Mota nvarchar(max)
	--,CONSTRAINT FK_Khoa_NguoiDung FOREIGN KEY (KhoaID) REFERENCES Khoa(Id)
)
GO
CREATE TABLE Phong
(
	Id int identity primary key,
	Ten nvarchar(max),
	DiaChi nvarchar(max),
	Gia decimal(18,2),
	TrangThaiID int,
	KhoaID int,
	GiamGiaBHYT int,
	--CONSTRAINT FK_Khoa_Phong FOREIGN KEY (KhoaID) REFERENCES Khoa(Id)
)
GO
ALTER TABLE Phong ADD LoaiPhong INT
GO
CREATE TABLE Giuong
(
	Id int identity primary key,
	Ten nvarchar(max),
	TrangThaiID int,
	PhongID int,
	--CONSTRAINT FK_Phong_Giuong FOREIGN KEY (PhongID) REFERENCES Phong(Id)
)
GO
CREATE TABLE Thuoc -- thuoc
(
	Id int identity primary key,
	Ten nvarchar(max), -- ten
	TrangThaiID int, -- trang thai
	Gia decimal(18,2), -- gia
	DonVi nvarchar(255), -- don vi
	CongTy nvarchar(max), -- cong ty
	MoTa nvarchar(max), -- noi dung cua thuoc su dung thanh phan , so luong , ham luong co trong thuoc
	GiamGiaBHYT int,
	AnhDaiDien varchar(max)
)
GO
CREATE TABLE Benh -- benh 
(
	Id int identity primary key,
	MaBenhICD varchar(50),
	Ten nvarchar(max),
	TrangThaiID int
)
GO
CREATE TABLE LoaiDichVu -- loai dich vu
(
	Id int identity primary key,
	MaLoaiDichVu varchar(50),
	Ten nvarchar(max),
	TrangThaiID int
)
GO
CREATE TABLE DichVu -- dich vu
(
	Id int identity primary key,
	MaDichVu varchar(50),
	Ten nvarchar(max),
	Gia decimal(18,2),
	TrangThaiID int,
	ApDungBHYT int,
	LoaiDichVuID int, -- loao dich vu
	--CONSTRAINT FK_LoaiDichVu_DichVu FOREIGN KEY (LoaiDichVuID) REFERENCES LoaiDichVu(Id),
)
GO
CREATE TABLE NhapVien -- ho so nhapp vien
(
	Id int identity primary key,
	MaBenhAn varchar(50), -- ma benh an
	NgayNhapVien datetime , -- ngay nhap vien
	NgayXuatVien datetime , -- ngay xuat vien ( trang thai = xuat vien => cap nhat )
	TrangThaiID int, -- trang thai
	NguoiDungID int , -- nguoi nhap vien
	--CONSTRAINT FK_NguoiDung_NhapVien FOREIGN KEY (NguoiDungID) REFERENCES NguoiDung(Id),
	KieuNhapVien int , -- kieu nhap vien : dung tuyen , trai tuyen , cap cuu
	ApDungBHYT int , -- loai trang thai su dung : dich vu , BHYT
)
GO
CREATE TABLE ChiTietNhapVien -- chi tiet nhap vien
(
	Id int identity primary key,
	NhapVienID int,
	--CONSTRAINT FK_NhapVien_ChiTietNhapVien FOREIGN KEY (NhapVienID) REFERENCES NhapVien(Id),
	KhoaID int, -- khoa
	PhongID int, -- phong
	TrangThaiID int,  -- trang thai
	NgayVaoKhoa datetime, -- ngay vao khoa
	ChuanDoanTuKhoaTruoc nvarchar(max), -- chuan doan tu khoa truoc ( neu co )
	ChuanDoanTamThoi nvarchar(max), -- chuan doan khoa nay
	BenhID int, -- benh chuan doan neu co
	--CONSTRAINT FK_ChiTietNhapVien_Benh FOREIGN KEY (BenhID) REFERENCES Benh(Id),
	BacSiID int, -- bac si 
	YTaID int , -- y ta
)


GO
CREATE TABLE TheoDoiBenhNhan -- so theo doi hang ngay
(
	Id int identity primary key,
	NhapVienID int,
	--CONSTRAINT FK_NhapVien_TheoDoiBenhNhan FOREIGN KEY (NhapVienID) REFERENCES NhapVien(Id),
	NgayTao datetime, -- ngay lap phieu
	NguoiTao int, -- nguoi lap phieu
	DienBienBenh nvarchar(max), -- dien bien
	YLenhBacSi nvarchar(max), -- y lenh bac si
	TrangThai int, -- trang thai 
)
GO
CREATE TABLE KhamBenh -- kham benh
(
	Id int identity primary key,
	MaSoKhamBenh varchar(50), -- ma y te
	NgayKham datetime , -- ngay kham benh
	NguoiDungID int , -- nguoi kham benh
	--CONSTRAINT FK_NguoiDung_KhamBenh FOREIGN KEY (NguoiDungID) REFERENCES NguoiDung(Id),
	TrangThaiID int, -- trang thai : hoan thanh , cho xet nghiem , chuyen tuyen, dang kham
	STT int, -- so thu tu kham chua benh
	KieuKhamBenh int , -- kieu nhap vien : dung tuyen , trai tuyen , cap cuu
	ApDungBHYT int , -- loai trang thai su dung : dich vu , BHYT
	ChiDanBacSi nvarchar(max) -- loi dan do cua bac si neu co
)
GO
CREATE TABLE ChiTietKhamBenh
(
	Id int identity primary key,
	KhoaID int, -- khoa
	PhongID int, -- phong
	KhamBenhID int, -- kham benh lan nao cua ai
	--CONSTRAINT FK_KhamBenh_ChiTietKhamBenh FOREIGN KEY (KhamBenhID) REFERENCES KhamBenh(Id),
	Mach float,
	NhipTho float,
	ChieuCao float,
	NhietDo float,
	HuyetAp float,
	CanNang float,
	Creatinin float,
	BMI float,
	KetQuaBanDau nvarchar(max), -- ket qua nhan dinh ban dau
	KetQuaTuTuyenTruoc nvarchar(max), -- ket qua tu tuyen truoc 
	LyDoChuyenTuyen nvarchar(max), -- ly do chuyen tuyen neu co
	ChuanDoanICD nvarchar(max), -- chuan doan benh ICD
	ChuanDoanBenh nvarchar(max), -- chuan doan khoa nay
	BenhID int, -- benh chuan doan code ICD neu co
	--CONSTRAINT FK_ChiTietKhamBenh_Benh FOREIGN KEY (BenhID) REFERENCES Benh(Id),
	StatusID int,  -- trang thai
	BacSiID int, -- bac si 
	YTaID int  -- y ta
)
GO
ALTER TABLE ChiTietKhamBenh ADD DoThanhThai float
ALTER TABLE ChiTietKhamBenh ALTER COLUMN HuyetAp varchar(50)
ALTER TABLE ChiTietKhamBenh ADD ChuanDoanNguyenNhan nvarchar(max)


GO
CREATE TABLE SuDungDichVu
(
	Id int identity primary key,
	MaPhieu varchar(50),
	NgayTao datetime,
	 TrangThaiID int,
	KhamBenhID int , -- don kham benh cua ai , bao gio
	--CONSTRAINT FK_KhamBenh_SuDungDichVu FOREIGN KEY (KhamBenhID) REFERENCES KhamBenh(Id),
	NhapVienID int, -- ho so benh an nao can su dung dich vu de tinh tien
	--CONSTRAINT FK_NhapVien_DichVu FOREIGN KEY (NhapVienID) REFERENCES NhapVien(Id),
)
GO
ALTER TABLE SuDungDichVu ADD BacSiID int
GO
CREATE TABLE ChiTietSuDungDichVu
(
	Id int identity primary key,
	NgayTao datetime,
	DichVuID int,
	SuDungDichVuID int,
	SoLuong int,
	TrangThaiID int
	--CONSTRAINT FK_DichVu_SuDungDichVu FOREIGN KEY (DichVuID) REFERENCES DichVu(Id),
)
GO
CREATE TABLE DonThuoc -- don thuoc su dung
(
	Id int identity primary key,
	MaDonThuoc varchar(50),
	NgayPhatThuoc datetime, -- ngay phat
	TrangThaiID int , -- trang thai da phat, chua phat .. 
	NhapVienID int,
	--CONSTRAINT FK_NhapVien_DonThuoc FOREIGN KEY (NhapVienID) REFERENCES NhapVien(Id),
	KhamBenhID int,
	--CONSTRAINT FK_KhamBenh_DonThuoc FOREIGN KEY (KhamBenhID) REFERENCES KhamBenh(Id),
)
GO
ALTER TABLE DonThuoc ADD BacSiID int
GO
CREATE TABLE ChiTietDonThuoc -- chi tiet don thuoc
(
	Id int identity primary key,
	ThuocID int,
	--CONSTRAINT FK_ThuocID_ChiTietDonThuoc FOREIGN KEY (ThuocID) REFERENCES Thuoc(Id),
	DonThuocID int,
	--CONSTRAINT FK_ChiTietDonThuoc_DonThuoc FOREIGN KEY (DonThuocID) REFERENCES DonThuoc(Id),
	SoLuong int,
	CachSuDung nvarchar(max), -- cach su dung
	HamLuongSuDung int , -- su dung bao lan / ngay
)
GO
CREATE TABLE AppConfig
(
	Id int identity Primary key
	, ImageLogin nvarchar(max)
	, ImagePanelLogin varchar(max)
)

GO
INSERT INTO AppConfig(ImageLogin,ImagePanelLogin)
VALUES ('~/Libs/Login_v3/images/bg-01.jpg','~-webkit-linear-gradient(top, #dee0f5, #0683ef)')
GO
INSERT INTO [NguoiDung](TenDangNhap,MatKhau,HoTen,TrangThaiID,LoaiNguoiDung)
     VALUES ('admin', N'/f9e7bsNi+c=',N'Adminstrator',1,0)
GO
-- bo sung ngay 13/11/2020
ALTER TABLE TheoDoiBenhNhan ADD Mach float
ALTER TABLE TheoDoiBenhNhan ADD NhipTho float
ALTER TABLE TheoDoiBenhNhan ADD ChieuCao float
ALTER TABLE TheoDoiBenhNhan ADD NhietDo float
ALTER TABLE TheoDoiBenhNhan ADD HuyetAp nvarchar(200)
ALTER TABLE TheoDoiBenhNhan ADD CanNang float
ALTER TABLE NhapVien ADD TLMienGiam float
GO
CREATE TABLE ChiTietGiuongBenh
(
	Id int identity primary key,
	ChiTietNhapVienID int,
	GiuongID int,
	NgayBatDau datetime,
	NgayKetThuc datetime,
	TrangThaiID int
)
GO
-- bo sung ngay 16/11/ 2020
ALTER TABLE [AppConfig] ADD ChiPhiKhamBenh float
ALTER TABLE NguoiDung ADD TLMienGiam float
ALTER TABLE [AppConfig] ADD TenTrungTamYTe nvarchar(max)
ALTER TABLE [AppConfig] ADD TenBenhVien nvarchar(max)
GO
CREATE TABLE ChiTietChiPhiNhapVien
(
	Id int identity primary key,
	NhapVienID int,
	SoTien int,
	StatusID int,
	NgayNopTien datetime
)
-- BO SUNG TOI 16/11/2020
GO
ALTER TABLE DonThuoc ADD LoaiDonThuoc int
-- bo sung 17/11/2020
ALTER TABLE [AppConfig] ADD Lisecenes nvarchar(max)
-- bo sung 23/11/2020
ALTER TABLE DichVu ADD TrangThietBi nvarchar(max)
